﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sensorsimu
{
    public partial class frmMainForm : Form
    {
        public frmMainForm()
        {
            InitializeComponent();
        }

        private void _AppendDisplay(string s)
        {
            txtOutput.Text += s;
        }

        private void frmMainForm_Load(object sender, EventArgs e)
        {
            _AppendDisplay("Welcome to sensor simulator\r\n");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            _AppendDisplay("Start...\r\n");
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _AppendDisplay("Stop\r\n");
        }
    }
}
