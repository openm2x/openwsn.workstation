
#include <windows.h>
#include <stdio.h>

void test02slave()
{
	// typedef int (__stdcall *TiFunSumPtr)(int a, int b);
	typedef int(__stdcall* TiFunIoSlaveTest)();

	HINSTANCE instance;
	// Load dLL dynamically
	instance = LoadLibrary(L"ioservice.dll"); 

	// GetProcAddress 获取该函数的地址
	TiFunIoSlaveTest io_slave_test = (TiFunIoSlaveTest)GetProcAddress(instance, "io_slave_test");
	int result = io_slave_test();
	printf("The result from dll is: %d", result);
	getchar();
}
