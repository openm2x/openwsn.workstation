﻿#include "avc_configall.h"
#include <stdio.h>
#include <string.h>
#ifdef CONFIG_LINUX
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#else
#include <winsock2.h>
#include <sys/types.h>
#endif
#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include <cassert>
#include <functional>
#include "avc_foundation.h"

// 调用方法
// . / udp_server  port   // 启动服务
// . / udp_client   server_ip  port   //客户端访问服务端
//
// Reference
// udp的server和client例子代码, 2018,
// https://blog.csdn.net/megan_free/article/details/85283902

#pragma comment(lib, "ws2_32.lib")

#define CONFIG_BUF_SIZE 256

void handle_udp_msg(int fd)
{
	char buf[CONFIG_BUF_SIZE];
#ifdef CONFIG_LINUX
	socklen_t len;
#else
	int len;
#endif
	int count, ret;
	struct sockaddr_in client_addr;


	struct timeval timeout;
	fd_set fds;

	while (1)
	{
		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		timeout.tv_sec = 60;
		timeout.tv_usec = 0;

		memset(buf, 0, CONFIG_BUF_SIZE);
		len = sizeof(client_addr);

		printf("select start......\n");

		ret = select(fd + 1, &fds, NULL, NULL, &timeout);

		printf("select end..........ret=%d.\n", ret);
#if 1
		if (ret == 0)
		{
			printf("select time out#######continue\n");
			continue;
		}

		if (ret == -1)
		{
			perror("select error");
			break;
		}
#endif
		if (FD_ISSET(fd, &fds))
			count = recvfrom(fd, buf, CONFIG_BUF_SIZE, 0, (struct sockaddr*) & client_addr, &len);
		if (count == -1)
		{
			printf("recieve data fail!\n");
			return;
		}

		printf("recv client:%s\n", buf);

		sendto(fd, buf, CONFIG_BUF_SIZE, 0, (struct sockaddr*) & client_addr, len);
	}
}

int echo_server_execute(int argc, char* argv[])
{
	int server_fd, ret;
	struct sockaddr_in ser_addr;

	if (argc != 2)
	{
		printf("Usage: %s <port> \n", argv[0]);
		return -1;
	}

#ifndef CONFIG_LINUX
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		printf("winsock initialization failed\n");
		return -2;
	}
#endif

	server_fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (server_fd < 0)
	{
		printf("create socket fail!\n");
		return -1;
	}

	memset(&ser_addr, 0, sizeof(ser_addr));
	ser_addr.sin_family = AF_INET;
	ser_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	ser_addr.sin_port = htons(atoi(argv[1]));

	ret = bind(server_fd, (struct sockaddr*) & ser_addr, sizeof(ser_addr));
	if (ret < 0)
	{
		printf("socket bind fail!\n");
		return -1;
	}

	handle_udp_msg(server_fd);

#ifdef CONFIG_LINUX
	close(server_fd);
#else
	closesocket(server_fd);
#endif

#ifndef CONFIG_LINUX
	WSACleanup();
#endif
	return 0;
}



#define BUF_SIZE 256

void udp_msg_sender(int fd, struct sockaddr* dst)
{
#ifdef CONFIG_LINUX
	socklen_t len;
#else
	int len;
#endif
	   
	struct sockaddr_in src;
	char message[BUF_SIZE];
	while (1)
	{
		len = sizeof(*dst);
		fputs("Input message:(输入Q退出):", stdout);
		memmove(&message[0], "testfdsaf", strlen("testfdsaf") + 1);
		// fgets(message, BUF_SIZE, stdin);
		if (!strcmp(message, "q\n") || !strcmp(message, "Q\n"))
			break;

		sendto(fd, message, BUF_SIZE, 0, dst, len);
		memset(message, 0, BUF_SIZE);
		recvfrom(fd, message, BUF_SIZE, 0, (struct sockaddr*) & src, &len);
		printf("recv server message:%s\n", message);
	}
}

int echo_client_execute(int argc, char* argv[])
{
	int client_fd;
	struct sockaddr_in ser_addr;

	if (argc != 3)
	{
		printf("Usage: %s <IP> <port> \n", argv[0]);
		return -1;
	}
	
#ifndef CONFIG_LINUX
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		printf("winsock initialization failed\n");
		return -1;
	}
#endif

	client_fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (client_fd < 0)
	{
		printf("create socket fail!\n");
		return -1;
	}

	memset(&ser_addr, 0, sizeof(ser_addr));
	ser_addr.sin_family = AF_INET;
	ser_addr.sin_addr.s_addr = inet_addr(argv[1]);
	ser_addr.sin_port = htons(atoi(argv[2]));

	udp_msg_sender(client_fd, (struct sockaddr*) & ser_addr);

#ifdef CONFIG_LINUX
	close(client_fd);
#else
	closesocket(client_fd);
#endif

#ifndef CONFIG_LINUX
	WSACleanup();
#endif

	return 0;
}

