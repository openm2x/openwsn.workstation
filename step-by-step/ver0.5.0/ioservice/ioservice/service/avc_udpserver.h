#ifndef _AVC_UDPSERVER_H_2143_
#define _AVC_UDPSERVER_H_2143_

#include "avc_configall.h"
#include "avc_foundation.h"


/* @modified by zhangwei on 2011.08.26
 * In order to comply the Windows .NET platform cross-calling standard, stdcall 
 * should be used. However, I cannot use keyword __stdcall here. So i had to change
 * to stdcall in project options:
 * 
 * 项目属性 => 配置属性 => C/C++ => 高级
 * Change calling convention to __stdcall (Default setting is __cdecl)
 */ 


class UDPServer {
private:
	WSADATA wsaData;
	SOCKET serverSocket;
	sockaddr_in serverSockAddr;

public:
	UDPServer();
	~UDPServer();
	void setSockAddr(const char* destAddr, const int destPort);
	void listen(const int port);
	void recv() const;
	int recv(char* buf, int capacity, int option);
};


#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif