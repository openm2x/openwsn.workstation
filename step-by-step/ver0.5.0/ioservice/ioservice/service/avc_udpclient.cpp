#include "avc_configall.h"
#include <iostream>
#include"avc_udpclient.h"


// Reference
// https ://blog.csdn.net/qwertyupoiuytr/java/article/details/54861318

using namespace std;

#pragma comment(lib, "ws2_32.lib")

UDPClient::UDPClient()
{
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	clientSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	cout << "Client Socketinitialized" << endl;
}

UDPClient::~UDPClient()
{
	closesocket(clientSocket);
	WSACleanup();
	cout << "Client Socketreleased" << endl;
}

void UDPClient::setSockAddr(const char* destAddr, const int destPort)
{
	clientSockAddr.sin_family = AF_INET;
	clientSockAddr.sin_port = htons(destPort);
	clientSockAddr.sin_addr.s_addr = inet_addr(destAddr);
}

void UDPClient::sendData(const char* buf, const int len) const
{
	sendto(clientSocket, buf, len, 0, (SOCKADDR*)&clientSockAddr, sizeof(clientSockAddr));
}
