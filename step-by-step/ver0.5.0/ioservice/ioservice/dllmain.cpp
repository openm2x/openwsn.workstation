// dllmain.cpp : 定义 DLL 应用程序的入口点。
//
#include "stdafx.h"
#include <assert.h>

#define DLL_IMPLEMENT 
#include "dllmain.h" 
 

#ifdef __cplusplus
  extern "C" {
#endif

// DLL_API int dll_api_test01(int x1, int x2)
int __stdcall dll_api_test01(int x1, int x2)
{
	return x1 + x2;
}
 
#ifdef __cplusplus
}
#endif 
