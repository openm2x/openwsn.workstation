#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// 在dll项目内部使用__declspec(dllexport)导出  
// 在dll项目外部使用时，用__declspec(dllimport)导入     
//
#ifdef DLL_IMPLEMENT  
#define DLL_API __declspec(dllexport)  
#else  
#define DLL_API __declspec(dllimport)  
#endif  

#ifdef __cplusplus
}
#endif 