#pragma once


#include <stdint.h>
#include <tchar.h> 


#ifdef __cplusplus
extern "C" {
#endif

int __stdcall io_master_test();


#ifdef __cplusplus
}
#endif