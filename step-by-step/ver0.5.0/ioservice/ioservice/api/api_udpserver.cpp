#include "api_configall.h"
#include "api_foundation.h"
#include "../service/avc_udpclient.h"
#include "../service/avc_udpserver.h"


static UDPServer* _server = NULL;

int __stdcall io_server_start()
{
	if (_server == NULL)
	{
		_server = new UDPServer();
	}
	return 1;
}

int __stdcall io_server_stop()
{
	if (_server != NULL)
	{
		delete _server;
		_server = NULL;
	}
	return 1;
}

int __stdcall io_server_recv(char* buf, int capacity, int option)
{
	assert(false);
	return _server->recv(buf, capacity, option); 
	return -1;
}

int __stdcall io_server_send(const char* buf, int len, int option)
{
	assert(false);
	return 1;
}
