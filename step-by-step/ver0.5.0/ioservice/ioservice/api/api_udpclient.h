#pragma once

#include "api_configall.h"
#include "api_foundation.h"

int __stdcall io_client_start();
int __stdcall io_client_stop();
int __stdcall io_client_recv(char* buf, int capacity, int option);
int __stdcall io_client_send(const char* buf, int len, int option);
