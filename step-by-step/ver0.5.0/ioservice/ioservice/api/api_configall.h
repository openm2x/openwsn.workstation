#ifndef _API_CONFIGALL_H_4784_
#define _API_CONFIGALL_H_4784_
/*******************************************************************************
 * @author zhangwei on 20070423
 * @name api_configall.h
 *
 * @note
 *
 * @modified by zhangwei on 200812
 ******************************************************************************/

#define DEBUG

#define CONFIG_DEBUG

#define CONFIG_UNICODE


// Set the whole project to be compiled as a DLL rather than a application.
#undef CONFIG_APPLICATION
#define CONFIG_DLL

#ifdef __cplusplus
extern "C" {
#endif
	
#ifdef CONFIG_DLL
  #define DLLFUNC _declspec(dllexport)
  #define DLLCLASS class __declspec (dllexport)
  #define MODU_INTF _declspec(dllexport)
#elif CONFIG_APPLICATION
  #define DLLFUNC _declspec(dllimport)
  #define DLLCLASS class __declspec (dllimport)
  #define MODU_INTF _declspec(dllimport)
#endif


#ifdef __cplusplus
}
#endif

/*****************************************************************************/
#endif

