#pragma once

#include "api_configall.h"
#include "api_foundation.h"

int __stdcall io_server_start();
int __stdcall io_server_stop();
int __stdcall io_server_recv(char* buf, int capacity, int option);
int __stdcall io_server_send(const char* buf, int len, int option);
