// libopenavc_test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "test01.h"
#include "test02.h"

int _tmain(int argc, _TCHAR* argv[])
{
	const int CONFIG_CHOICE = 1;

	switch (CONFIG_CHOICE)
	{
	case 1:
		test01(argc, argv);
		break;
	case 2:
		test02();
		break;
	}

	return 0;
}
