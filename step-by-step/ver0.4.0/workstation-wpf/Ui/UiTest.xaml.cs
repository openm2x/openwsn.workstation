﻿using App.WorkStation.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace App.WorkStation.Ui
{
    /// <summary>
    /// UiTest.xaml 的交互逻辑
    /// </summary>
    public partial class UiTest : Window
    {
        public UiTest()
        {
            InitializeComponent();
        }

        private void TestDllButton_Click(object sender, RoutedEventArgs e)
        {
            var io = new TiAppIoService();
            int result = io.Sum(3, 5);
            MessageBox.Show(result.ToString(), "DLL Test");
        }

        private void MessageBoxTestButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("内容", "标题", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                MessageBox.Show("yes", "Demo");
            }
            else
            {
                MessageBox.Show("no", "Demo");
            }

        }
    }
}
