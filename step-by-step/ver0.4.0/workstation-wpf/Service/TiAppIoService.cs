﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace App.WorkStation.Service
{
    public class TiAppIoService
    {
        public TiAppIoService()
        {

        }

        public int Sum(int x1, int x2)
        {
            return _sum(x1, x2);
        }

        //[DllImport("ioservice.dll", EntryPoint= "dll_api_test01", CallingConvention=CallingConvention.StdCall)]
        [DllImport("ioservice.dll", EntryPoint = "dll_api_test01")]
        public static extern int _sum(int x1, int x2);
    }
}
