﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WorkStation.Service
{
    /// <summary>
    /// Main Application Service
    /// 
    /// Reference
    /// c# 几种singleton 实现,
    /// https://www.cnblogs.com/scotth/p/10450243.html
    /// https://www.manning.com/books/c-sharp-in-depth-fourth-edition
    /// http://csharpindepth.com/Articles/General/Singleton.aspx#introduction
    /// 
    /// History
    /// author zhangwei on 2020.06.20
    /// - Add this class and singleton implementations. Considering the thread safety.
    /// </summary>
    public class TiAppService
    {
        // For threadsafe considerations
        private static TiAppService _instance = null;
        private static readonly object _padlock = new object();

        private TiAppIoService _io;
        private TiAppDataService _data;

        private TiAppService()
        {
            _io = new TiAppIoService();
            _data = new TiAppDataService();
        }

        public static TiAppService Instance
        {
            get
            {
                lock (_padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new TiAppService();
                    }
                    return _instance;
                }
            }
        }

        public TiAppIoService GetAppIoService()
        {
            return _io;
        }
    }
}
