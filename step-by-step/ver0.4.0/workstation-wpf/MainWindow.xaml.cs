﻿using App.WorkStation.Service;
using App.WorkStation.Ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace workstation_wpf
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// 
    /// author zhangwei@TongJi University, 2020
    /// 
    /// </summary>
    public partial class MainWindow : Window
    {
        private TiAppService _avc = TiAppService.Instance;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            String s = "Welcome to come to the simulated sensor and workstation application in the embedded course 2020 at TongJi University!";
            OutputTextBlock.Text = s;
        }

        private void _AppendDisplay(string s)
        {
            OutputTextBlock.Inlines.Add(new LineBreak());
            OutputTextBlock.Inlines.Add(s);
        }

        private void ExecuteButton_Click(object sender, RoutedEventArgs e)
        {
            ProgressBar.Minimum = 0;
            ProgressBar.Maximum = 100;
            ProgressBar.Value = 50;
        }

        private void TestButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("内容", "标题", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                // Demonstrate how to call the DLL method.
                int result = _avc.GetAppIoService().Sum(3, 5);
                _AppendDisplay("yes" + result.ToString());
            }
            else
            {
                _AppendDisplay("no");
            }
        }

        private void TestWindowButton_Click(object sender, RoutedEventArgs e)
        {
            var testwindow = new UiTest();
            testwindow.ShowDialog();
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            var about = new UiAbout();
            about.ShowDialog();
        }
    }
}
