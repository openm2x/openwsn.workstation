#ifndef _AVC_IO4RS232_H_2143_
#define _AVC_IO4RS232_H_2143_

/* Library for RS232 serial port manipulation with frame filter capability.
 * @author Zhang Wei (TJU) on 2011.07.28
 */ 
#include <stdint.h>
#include <tchar.h> 

/* Reference
 * - 从 DLL 导出, http://msdn.microsoft.com/zh-cn/library/z4zxe9k8(v=VS.80).aspx
 * - 确定要使用的导出方法, 
 *   http://msdn.microsoft.com/zh-cn/library/900axts6(v=VS.80).aspx
 * - 使用 DEF 文件从 DLL 导出, 
 *   http://msdn.microsoft.com/zh-cn/library/d91k01sh(v=VS.80).aspx
 * - 使用 __declspec(dllexport) 从 DLL 导出, 
 *   http://msdn.microsoft.com/zh-cn/library/a90k134d(VS.80).aspx
 * - 导出 C++ 函数以用于 C 语言可执行文件, 
 *   http://msdn.microsoft.com/zh-cn/library/wf2w9f6x(v=VS.80).aspx
 * - 导出 C 函数以用于 C 或 C++ 语言可执行文件, 
 *   http://msdn.microsoft.com/zh-cn/library/ys435b3s(v=VS.80).aspx 
 */
 

/**
 * This module defined the interface of librs232 dynamic link library
 * which can be used in other program files such as C# or LabView.
 */

#ifdef __cplusplus
extern "C" {
#endif

/* @modified by zhangwei on 2011.08.26
 * In order to comply the Windows .NET platform cross-calling standard, stdcall 
 * should be used. However, I cannot use keyword __stdcall here. So i had to change
 * to stdcall in project options:
 * 
 * 项目属性 => 配置属性 => C/C++ => 高级
 * Change calling convention to __stdcall (Default setting is __cdecl)
 */ 
int __stdcall filter_50hz(int * input, int inputlen, int * output, int outputlen, int samplerate);

#ifdef __cplusplus
}
#endif

#endif
