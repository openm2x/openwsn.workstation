#pragma once;  

// -----------------------------------------------------------------------------
// History
// author zhangwei on 2020.06.20
// - Create the module.
// -----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

#include "foundation.h"

// DLL_API int dll_api_test01(int x1, int x2);
int dll_api_test01(int x1, int x2);

#ifdef __cplusplus
}
#endif 