﻿using SensorSimu.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorSimu
{
    public partial class frmMainForm : Form
    {
        Service.TiAppService _avc = null;

        public frmMainForm()
        {
            InitializeComponent();
            _avc = new TiAppService();
        }

        private void _AppendDisplay(string s)
        {
            txtOutput.Text += s;
        }

        private void frmMainForm_Load(object sender, EventArgs e)
        {
            _AppendDisplay("Welcome to sensor simulator\r\n");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            _AppendDisplay("Start...\r\n");
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _AppendDisplay("Stop\r\n");
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            // TODO
            // modified by zhangwei on 2020.06.21
            // This application uses the most simple method to drive the I/O process to run.
            // It uses the timer components. 
            //
            // But but this is NOT a good idea!!!
            // The most approriate method is to use a seprate Thread for I/O processing 
            // (including BackgroudWorker. Background Worker is also a special kind of process.)
            //
            // Timer is only for demonstration. It cannot be used in real engineering applications.
            //
            _avc.Evolve();

            while (true)
            {
                string text = _avc.ReadConsoleQueue();
                if (string.IsNullOrEmpty(text))
                    break;
                txtOutput.AppendText(text);
            }
        }
    }
}
