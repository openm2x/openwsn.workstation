﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SensorSimu.Service
{
    // C#调用C++DLL传递结构体数组的终极解决方案， 2013，
    // https://blog.csdn.net/xxdddail/article/details/11781003
    public class TiAppNetIoService
    {
        public TiAppNetIoService()
        {
        }

        public void Start()
        {

        }

        public void Stop()
        {

        }

        public byte[] Recv()
        {
            return null;
        }

        public int Send(byte[] buf)
        {
            return 0;
        }

        public void Test()
        {

        }

        // The following three methods are only for demo purpose.

        //[DllImport("ioservice.dll", EntryPoint= "dll_api_test01", CallingConvention=CallingConvention.StdCall)]
        [DllImport("ioservice.dll", EntryPoint = "dll_api_test01")]
        public static extern int Sum(int x1, int x2);

        //[DllImport("ioservice.dll", EntryPoint= "dll_api_test01", CallingConvention=CallingConvention.StdCall)]
        [DllImport("ioservice.dll", EntryPoint = "io_service_execute")]
        public static extern int IoServiceExecute();

        //[DllImport("ioservice.dll", EntryPoint= "dll_api_test01", CallingConvention=CallingConvention.StdCall)]
        [DllImport("ioservice.dll", EntryPoint = "io_client_execute")]
        public static extern int IoClientExecute();
    }
}
