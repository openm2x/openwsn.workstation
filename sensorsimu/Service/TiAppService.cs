﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorSimu.Service
{
    public class TiAppService
    {
        TiAppNetIoService _nio = null;
        TiAppSensorService _sensors = null;
        Queue<string> _consolebuffer;

        public TiAppService()
        {
            _nio = new TiAppNetIoService();
            _sensors = new TiAppSensorService();
            _consolebuffer = new Queue<string>();
        }

        public TiAppNetIoService GetAppNetIoService()
        {
            return _nio;
        }

        public TiAppSensorService GetSensorService()
        {
            return _sensors;
        }

        /// <summary>
        /// TODO: This method must be called periodically to drive the whole I/O process to run.
        /// In this version, I uses the timer component in the UI layer's MainForm. But this is
        /// not a good idea. Generally, we should place the call to this method in a thread.
        /// 
        /// TODO: you should replace request with a queue to avoid data losing.
        /// 
        /// </summary>
        public void Evolve()
        {
            byte[] request = _nio.Recv();
            byte[] response = new byte[512];

            if (request.Length > 0)
            {
                // This method also save the received request and the response into the 
                // console queue. And the main form will read the queue and show these
                // data in the UI.
                //
                WriteConsoleQueue("request: ", request, request.Length, "\r\n");
                if (Interprete(request, ref response) > 0)
                {
                    WriteConsoleQueue("request: ", request, request.Length, "\r\n");
                    _nio.Send(response);
                }
            }
        }

        protected int Interprete(byte[] input, ref byte[] output)
        {
            // TODO: 
            for (int i = 0; i < input.Length; i++)
                output[i] = input[i];

            return 1;
        }

        public string ReadConsoleQueue()
        {
            string line = "";
            if (_consolebuffer.Count > 0)
            {
                line = _consolebuffer.Dequeue();
            }
            return line;
        }

        protected void WriteConsoleQueue(string prefix, byte[] buf, int len, string suffix)
        {
            string text = prefix + ByteArrayToHexStr(buf, len) + suffix;
            _consolebuffer.Enqueue(text);
        }

        //字节数组转16进制字符串
        private static string ByteArrayToHexStr(byte[] bytes, int length)
        {
            StringBuilder returnStr = new StringBuilder();
            if (bytes != null)
            {
                for (int i = 0; i < length; i++)
                {
                    returnStr.Append(bytes[i].ToString("X2")).Append(' ');
                    if (i % 25 == 0)
                        returnStr.Append("\r\n");
                }
            }
            return returnStr.ToString();
        }
    }
}
