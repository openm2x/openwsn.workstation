@echo off
@echo ===============================================================================
@echo This script is developed by Zhang Wei(TongJi University) to help deleting
@echo temperrary filed during the developing.
@echo .  
@echo author Zhang Wei (BigLittle Group, TongJi University, 2019)
@echo ===============================================================================

set rootdir=\dev\openm2m\source\workstation-wpf\

rem set file=%rootdir%addon\delete_temp_obj_files.bat
rem call %file%
rem pause

rem set file=%rootdir%component\delete_temp_obj_files.bat
rem call %file%
rem pause

rem set file=%rootdir%extension\delete_temp_obj_files.bat
rem call %file%
rem pause

set curdir=%rootdir%\admin\obj\
cd %curdir%
@echo start to delete all files under the following directory:
@echo %curdir%
set files=%curdir%*.*
del %files% /q
rd /s/q Debug
pause

set curdir=%rootdir%\init\obj\
cd %curdir%
@echo start to delete all files under the following directory:
@echo %curdir%
set files=%curdir%*.*
del %files% /q
rd /s/q Debug
pause

set curdir=%rootdir%libmidware\obj\
cd %curdir%
@echo start to delete all files under the following directory:
@echo %curdir%
set files=%curdir%*.*
del %files% /q
rd /s/q Debug
pause

set curdir=%rootdir%workstation\obj\
cd %curdir%
@echo start to delete all files under the following directory:
@echo %curdir%
set files=%curdir%*.*
del %files% /q
rd /s/q Debug
pause

cd %rootdir%

@echo Done. Please attention whether there're errors. Good luck! (by zhangwei)

pause
