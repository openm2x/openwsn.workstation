﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Xvx.Foundation
{
    public class TiEvent
    {
        private int id;
        private Object source;
        private Object destination;
        private Object param;        
        private string message;

        public int Id { get => id; set => id = value; }
        public string Message { get => message; set => message = value; } // TODO: TO BE DELETED
        public object Source { get => source; set => source = value; }
        public object Destination { get => destination; set => destination = value; }
        public object Param { get => param; set => param = value; }

        public TiEvent()
        {
            this.id = 0;
            this.source = null;
            this.destination = null;
            this.param = null;
        }

        public TiEvent(int id)
        {
            this.id = id;
            this.source = null;
            this.destination = null;
            this.param = null;
        }

        public TiEvent(int id,string message)
        {
            this.id = id;
            this.message = message;
            this.source = null;
            this.destination = null;
            this.param = null;
        }

        public TiEvent(int id, Object source, Object destination)
            : this(id, source, destination, null)
        {
        }

        public TiEvent(int id, Object source, Object destination, object param)
        {
            this.id = id;
            this.source = source;
            this.destination = destination;
            this.param = param;
        }

        public void AssignFrom(TiEvent e)
        {
            if (e != null)
            {
                this.id = e.id;
                this.message = e.message;
                this.source = e.source;
                this.destination = e.destination;
                this.param = e.param;
            }
        }

        public void Clear()
        {
            this.id = TiFoundation.EVENT_UNKNOWN;
            this.message = null;
            this.source = null;
            this.destination = null;
            this.param = null;
        }

        public TiEvent Clone()
        {
            return (TiEvent)MemberwiseClone();
        }
    }
}
