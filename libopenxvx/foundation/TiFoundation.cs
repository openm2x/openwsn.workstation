﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Xvx.Foundation
{

    /// <summary>
    /// interpretor for TiEventId
    /// 16进制4位，采取高两位代表模块，低两位代表模块内具体事件，每个模块256
    /// </summary>
    public class TiFoundation
    {

        public const int NONE = 0x0000;
        public const int UM_APP_MESSAGE = 0x6000;

        public const int EVENT_UNKNOWN = UM_APP_MESSAGE + 0x0001;
        public const int EVENT_MESSAGE_PENDING = UM_APP_MESSAGE + 0x0002;
        public const int EVENT_DATA_READY = UM_APP_MESSAGE + 0x0003;
        public const int EVENT_CACHE_NEWDATA_ACCEPTED = UM_APP_MESSAGE + 0x0004;
        public const int EVENT_PROGRESS = UM_APP_MESSAGE + 0x0005;
        public const int EVENT_END_OF_EXECUTION = UM_APP_MESSAGE + 0x0006;
        public const int EVENT_CANCEL = UM_APP_MESSAGE + 0x0007;

        public const int EVENT_SCHE_TASK_PROGRESS = UM_APP_MESSAGE + 0x0010;
        public const int EVENT_SCHE_TASK_COMPLETED = UM_APP_MESSAGE + 0x0011;
        public const int EVENT_SCHE_TASK_ERROR = UM_APP_MESSAGE + 0x0012;
        public const int EVENT_SCHE_TASK_CANCELED = UM_APP_MESSAGE + 0x0013;
        public const int EVENT_SCHE_REPORT = UM_APP_MESSAGE + 0x0014;

        public const int EVENT_SHOW_DATASET_MANAGER = UM_APP_MESSAGE + 0x0015;

        #region Message Identifier Definition
        public const int MSG_UNKNOWN = 0;

        // When the application received the NOTIFIER or PROGRESS message, 
        // it can directly show them to the user and continue, or just ignore it.
        //
        public const int MSG_NOTIFY = 1;
        public const int MSG_PROGRESS = 2;

        public const int MSG_COMPLETED = 3;
        public const int MSG_CANCELED = 4;
        public const int MSG_ERROR = 5;
        public const int MSG_EXCEPTION = 9;
        public const int MSG_START = 6;
        public const int MSG_STOP = 7;
        public const int MSG_SCHEDULER_CANCELED = 8;
        public const int MSG_SCHEDULER_COMPLETED = 10;
        #endregion


    }
}
