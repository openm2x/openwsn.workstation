﻿using App.Xvx.Common;

namespace App.Xvx.Foundation
{
    public interface IiIoHandler
    {
        int Execute(TiIoBuf input, TiIoBuf output);
    }
}
