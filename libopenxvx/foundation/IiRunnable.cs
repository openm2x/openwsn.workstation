﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Xvx.Foundation
{
    /// <summary>
    /// Runnable is the abstraction of thread or process or schedulable service 
    /// in the system.
    /// </summary>
    public interface IiRunnable
    {
        int Execute(object o);
    }
}
