﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace App.Xvx.Foundation
{
    /// <summary>
    /// TiThreadWorker is an high level encapsulation of the standard Thread class.
    /// 
    /// modified by zhangwei on 2019.01.29
    /// </summary>
    public class TiThread
    {
        public delegate int TiWorkerRunnerDelegation(object argument);
        public delegate void TiOnWorkerOutputDelegation(TiWorkerEvent e, object o);

        private delegate void TiOnThreadOutputDelegation(string str);
        private delegate void TiOnThreadCompleteDelegation(string str);

        public enum TiWorkerEvent { ERROR = 0, OUTPUT, PROGRESS, COMPLETED };

        private object _argument;
        private TiWorkerRunnerDelegation _runner;
        private TiOnWorkerOutputDelegation _onworkeroutput;

        //TiCmdPythonInterpreter _interpreter;
        Thread _thread;

        public TiThread(TiWorkerRunnerDelegation runner, object argument =null, TiOnWorkerOutputDelegation onworkeroutput=null)
        {
            _runner = runner;
            _argument = argument;

            if (onworkeroutput == null)
                _onworkeroutput = OnWorkerOutput;
            else
                _onworkeroutput = onworkeroutput;
        }

        public TiThread(IiRunnable runnable, object argument = null, TiOnWorkerOutputDelegation onworkeroutput = null)
        {
            _runner = runnable.Execute;
            _argument = argument;

            if (onworkeroutput == null)
                _onworkeroutput = OnWorkerOutput;
            else
                _onworkeroutput = onworkeroutput;
        }

        public void Start()
        {
            // 把方法赋值给委托
            TiOnThreadCompleteDelegation callback = new TiOnThreadCompleteDelegation(OnThreadComplete);
            // Action<string> callback = ((string str) => { Console.WriteLine(str); });

            ParameterizedThreadStart init = new ParameterizedThreadStart(_ThreadRunner);
            Console.WriteLine("BigLittle.Ext.PythonAnalyzer: Creating the Child thread");
            _thread = new Thread(init);
            _thread.Start(_argument);
            // Console.ReadKey();
        }

        public void Stop()
        {
            _thread.Abort();
        }

        private void _ThreadRunner(object argument)
        {
            // _interpreter = new TiCmdPythonInterpreter();

            try
            {
                Console.WriteLine("Child thread starts");
                _runner(argument);
                Thread.Sleep(10);
            }
            catch (ThreadAbortException)
            {
                Console.WriteLine("TiThreadWorker: Abort Exception");
            }
            catch (Exception e2)
            {
                Console.WriteLine("TiThreadWorker: Unexpected exception => " + e2.Message);
            }
            finally
            {
                //Console.WriteLine("Couldn't catch the Thread Exception");
            }

            // _interpreter = null;
            System.GC.Collect();
        }

        // 用于线程回调的方法，但实质上是在主线程中执行的
        private void OnThreadComplete(string str)
        {
            OnWorkerOutput(TiWorkerEvent.COMPLETED, null);
            Console.WriteLine(str);
        }

        public int Interprete(Dictionary<string, object> input, Dictionary<string, object> output)
        {
            return 1;
        }

        protected void OnWorkerOutput(TiWorkerEvent e, object argument=null)
        {
        }
    }
}
