﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Xvx.Foundation
{
    /// <summary>
    /// Byte array based I/O buffer. It also support the queue behavior (FIFO).
    /// TODO using array to re-write this class or extends Queue<byte>?
    /// </summary>
    public class TiIoBuf
    {
        private byte[] _array;
        private int _head;
        private int _tail;
        private int _size;

        private int _growFactor = 200;

        private const int _MinimumGrow = 16;

        public TiIoBuf(int capacity = 1024)  //构造器
        {
            if (capacity < 0)
            {
                throw new ArgumentOutOfRangeException("capacity must be positive");
            }

            _array = new byte[capacity];
            _head = 0;
            _tail = 0;
            _size = 0;
        }


        public int Length
        {
            get => this._size;
        }

        /// <summary>
        /// 返回复制的TiIoBuf
        /// </summary>
        /// <returns></returns>
        public TiIoBuf Duplicate()
        {
            TiIoBuf buf = new TiIoBuf(_size)
            {
                _size = _size
            };

            buf._array = this.Peek(_size);
            return buf;

        }

        public void Clear()
        {
            _head = 0;
            _tail = 0;
            _size = 0;
        }


        /// <summary>
        /// 队尾插入一个元素
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public void Push(byte value)
        {
            if (_size == _array.Length)
            {
                int newcapacity = (int)(_array.Length * (long)_growFactor / 100);
                if (newcapacity < _array.Length + _MinimumGrow)
                {
                    newcapacity = _array.Length + _MinimumGrow;
                }
                SetCapacity(newcapacity);
            }
            _array[_tail] = value;
            _tail = (_tail + 1) % _array.Length;
            _size++;
        }


        public void Push(byte[] value)
        {
            if (_size + value.Length > _array.Length)
            {
                int adjust = Math.Max(value.Length, _array.Length);
                int newcapacity = (int)(adjust * (long)_growFactor / 100);

                if (newcapacity < _array.Length + _MinimumGrow)
                {
                    newcapacity = _array.Length + _MinimumGrow;
                }
                SetCapacity(newcapacity);
            }

            int numToCopy = value.Length;
            int firstPart = (_array.Length - _tail < numToCopy) ? _array.Length - _tail : numToCopy;
            Array.Copy(value, 0, _array, _tail, firstPart);
            numToCopy -= firstPart;
            if (numToCopy > 0)
                Array.Copy(value, _array.Length - _tail, _array, 0, numToCopy);

            _tail = (_tail + value.Length) % _array.Length;
            _size += value.Length;
        }

        public byte Peek()
        {
            if (_size == 0)
            {
                throw new InvalidOperationException("no data in buf");
            }
            return _array[_head];
        }

        public byte[] Peek(int len)
        {
            if (len > _size)
            {
                throw new InvalidOperationException("no enough data in buf");
            }

            byte[] data = new byte[len];
            int numToCopy = len;
            int firstPart = (_array.Length - _head < numToCopy) ? _array.Length - _head : numToCopy;
            Array.Copy(_array, _head, data, 0, firstPart);
            numToCopy -= firstPart;
            if (numToCopy > 0)
            {
                Array.Copy(_array, 0, data, _array.Length - _head, numToCopy);
            }

            return data;
        }

        /// <summary>
        /// retun the first value of queue
        /// </summary>
        /// <returns></returns>
        public byte Pop()
        {
            if (_size == 0)
            {
                throw new InvalidOperationException("no data in buf");
            }
            byte removed = _array[_head];
            //_array[_head] = 0; no need to release
            _head = (_head + 1) % _array.Length;
            _size--;
            return removed;
        }

        public byte[] Pop(int len)
        {
            byte[] data = this.Peek(len);

            _head = (_head + len) % _array.Length;
            _size -= len;

            return data;
        }


        public void Append(TiIoBuf buf)
        {
            byte[] data = buf.Peek(buf._size);
            this.Push(data);
        }

        private void SetCapacity(int capacity)
        {
            byte[] newarray = new byte[capacity];
            if (_size > 0)
            {
                if (_head < _tail)
                {
                    Array.Copy(_array, _head, newarray, 0, _size);
                }
                else
                {
                    Array.Copy(_array, _head, newarray, 0, _array.Length - _head);
                    Array.Copy(_array, 0, newarray, _array.Length - _head, _tail);
                }
            }

            _array = newarray;
            _head = 0;
            _tail = (_size == capacity) ? 0 : _size;
        }
    }
}
