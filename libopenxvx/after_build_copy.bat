@echo off
@echo ===============================================================================
@echo This script is developed by Zhang Wei(TongJi University) to help copying files
@echo to the developing folders of other projects. It helps to keep file version synchronization
@echo among multiple projects.
@echo 
@echo author Zhang Wei (BigLittle Group, TongJi University, 2019)
@echo ===============================================================================

set rootdir=\dev\uvlab-workstation\source\uvlab.workstation-xvx\
set dirfrom=%rootdir%libopenxvx\bin\Debug\
set fullfile1=%dirfrom%libopenxvx.*

set tempdir=\dev\uvlab-workstation\source\uvlab.workstation-dr\
set dirto1=%tempdir%bin\
set dirto2=%tempdir%install\current\bin\
set dirto3=%tempdir%root\bin\

set tempdir=\dev\uvlab-workstation\source\uvlab.ioservice\
set dirto4=%tempdir%bin\
set dirto5=%tempdir%install\current\bin\
set dirto6=%tempdir%root\bin\

set tempdir=\dev\uvlab-workstation\source\uvlab.workstation-ext\
set dirto7=%tempdir%bin\
set dirto8=%tempdir%install\current\bin\
set dirto9=%tempdir%root\bin\

@echo copy files from
@echo %fullfile1%
@echo copy files to
@echo %dirto1%
@echo %dirto2%
@echo %dirto3%
@echo %dirto4%
@echo %dirto5%
@echo %dirto6%
@echo %dirto7%
@echo %dirto8%
@echo %dirto9%

pause

@echo 
@echo
@echo Currently copying the following files to their destination
@echo %fullfile1%
copy %fullfile1% %dirto1% 
copy %fullfile1% %dirto2%
copy %fullfile1% %dirto3% 
copy %fullfile1% %dirto4% 
copy %fullfile1% %dirto5% 
copy %fullfile1% %dirto6% 
copy %fullfile1% %dirto7% 
copy %fullfile1% %dirto8% 
copy %fullfile1% %dirto9% 

set tempdir=\dev\openedu-workstation\source\openedu.workstation-wpf\
set dirto1=%tempdir%bin\
set dirto2=%tempdir%install\current\bin\
set dirto3=%tempdir%root\bin\

copy %fullfile1% %dirto1% 
copy %fullfile1% %dirto2%
copy %fullfile1% %dirto3% 

@echo Done. Please attention whether there're errors. Good luck! (by zhangwei)

;rem Here's the original Visual Studio After Bulid script
;
;rem copy "$(TargetDir)$(ProjectName).*" "$(SolutionDir)bin\"
;rem copy "$(TargetDir)$(ProjectName).*" "$(SolutionDir)install\current\common\bin\*.*"
;rem copy "$(TargetDir)$(ProjectName).*" "$(SolutionDir)root\bin\"
;rem copy "$(TargetDir)*.*" "$(SolutionDir)install\current\common\bin\*.*"
;rem copy "$(TargetDir)$(ProjectName).*" "..\..\..\..\uvlab.ioservice\bin\"
;rem -- copy "$(TargetDir)$(ProjectName).*" "..\..\..\..\uvlab.ioservice\install\current\common\bin\"
;rem copy "$(TargetDir)$(ProjectName).*" "..\..\..\..\uvlab.workstation-ext\bin\"
;rem -- copy "$(TargetDir)$(ProjectName).*" "..\..\..\..\uvlab.workstation-ext\install\current\common\bin"
;rem C:\Dev\uvlab-workstation\source\uvlab.ioservice\install\current\common\bin\libopenxvx.dll

pause