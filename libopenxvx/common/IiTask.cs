﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Xvx.Foundation;

namespace App.Xvx.Common
{
    public enum TiTaskState { INIT = 0, IDLE, READY, RUNNING, ERROR, COMPLETED }

    public interface IiTask
    {
        /// <summary>
        /// 定时任务执行的方法
        /// </summary>
        void Evolve(TiEvent e);

        TiTaskState State();

        /// <summary>
        /// 清理释放占用的资源
        /// </summary>
        void Clear();
    }
}
