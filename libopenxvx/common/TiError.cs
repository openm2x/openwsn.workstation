﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Xvx.Common
{
    public class TiError
    {
        public int Code;
        public string Message;

        public TiError()
        {
            Clear();
        }

        public void Clear()
        {
            Code = 0;
            Message = null;
        }

        public void Set(TiError error)
        {
            if (error == null)
            {
                this.Code = 0;
            }
            else
            {
                this.Code = error.Code;
                this.Message = error.Message;
            }
        }

        public void Set(int code, string message)
        {
            this.Code = code;
            this.Message = message;
        }
    }

}
