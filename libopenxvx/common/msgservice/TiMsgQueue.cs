﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Xvx.Foundation;
using App.Xvx.Common;

namespace App.Xvx.Common.MsgService
{
    public class TiMsgQueueItem<T>
    {
        public T Value { get; set; }
        public DateTime Timestamp { get; set; }

        public TiMsgQueueItem()
        {
            this.Value = default(T);
            this.Timestamp = DateTime.Now;
        }

        public TiMsgQueueItem(T o)
        {
            this.Value = o;
            this.Timestamp = DateTime.Now;
        }

        public void Clear()
        {
            Timestamp = default(DateTime);
        }

        public string Dump()
        {
            string s = string.Format("{0}: {1}\r\n", this.Value, this.Timestamp);
            return s;
        }
    }

    public class TiMsgQueue<T>
    {
        public const int CAPACITY = 500;

        protected ConcurrentQueue<TiMsgQueueItem<T>> _queue;
        private int _capacity;
        private int _count = 0;
        private TiMsgQueueItem<T> _tmpitem;

        public TiMsgQueue() : this(CAPACITY)
        {
        }

        public TiMsgQueue(int capacity) 
        {
            _queue = new ConcurrentQueue<TiMsgQueueItem<T>>();
            _capacity = capacity;
            _count = 0;
            _tmpitem = TiMetaObjectService.CreateInstance<TiMsgQueueItem<T>>();
            // _tmp = default(T);

            // attention: It seems the concurrent queue may have an unexpect item in it
            // at first. So tried the following to clear the queue.
            Clear();
        }

        public bool IsEmpty()
        {
            return _queue.IsEmpty;
        }

        public int Capacity()
        {
            return _capacity;
        }

        // protected static T CreateNewInstance<T>() where T : new()
        protected static T2 CreateNewInstance<T2>() where T2 : new()
        {
            return new T2();
        }

        public int Push(T msg)
        {
            // TODO
            // lock (this)
            {

            }
            /*
            if (_count >= _capacity)
            {
                Debug.Assert(false, "This should rarely happen!");
                // T o = TiMetaObjectService.CreateInstance<T>();
                _queue.TryDequeue(out _tmpitem);
                _count--;
            }
            */

            int retvalue = 0;

            if (_count < _capacity)
            {
                // TiMessage msg = new TiMessage(addrfrom, addrto, msgid, msgtext, e, tag);
                _queue.Enqueue(new TiMsgQueueItem<T>(msg));
                _count++;
                retvalue = 1;
            }

            return retvalue;
        }

        public bool TryPeek(out TiMsgQueueItem<T> item)
        {
            return _queue.TryPeek(out item);
        }

        public bool TryPeek(out T msg)
        {
            if (_queue.TryPeek(out _tmpitem))
            {
                msg = _tmpitem.Value;
                return true;
            }
            else
            {
                msg = default(T);
                return false;
            }

            // return _queue.TryPeek(out item);
        }

        public bool TryDequeue(out TiMsgQueueItem<T> item)
        {
            // TODO
            // lock
            bool retvalue = true;
            if (_queue.TryDequeue(out item))
            {
                _count--;
                if (_count < 0)
                    _count = 0;
            }
            else
                retvalue = false;
            return retvalue;
        }

        public bool TryDequeue(out T msg)
        {
            // TODO
            // lock
            bool retvalue = true;
            if (_queue.TryDequeue(out _tmpitem))
            {
                msg = _tmpitem.Value;
                _count--;
                if (_count < 0)
                    _count = 0;
            }
            else
            {
                msg = default(T);
                retvalue = false;
            }

            return retvalue;
        }

        public void Clear()
        {
            if (_queue != null)
            {
                try
                {
                    // TiMessage msg = new TiMessage();
                    while (!_queue.IsEmpty)
                    {
                        _queue.TryDequeue(out _tmpitem);
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        public string Dump()
        {
            StringBuilder sb = new StringBuilder("TiMsgQueue: \r\n");

            foreach (var item in _queue)
            {
                sb.AppendFormat("\t{0}\r\n", item.ToString());
            }

            return sb.ToString();
        }
    }

}
