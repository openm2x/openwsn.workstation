﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Xvx.Foundation;

namespace App.Xvx.Common.MsgService
{
    public class TiMessage
    {
        protected TiEvent _event;

        public string AddrFrom { get; set; }
        public string AddrTo { get; set; }
        public int Id { get; set; }
        public int IntValue { get; set; }
        public TiEvent Event { get=>_event; set=>_event=value; }
        public string Text { get; set; }
        public Object Object { get; set; }

        public TiMessage()
            : this(null, null, TiFoundation.EVENT_MESSAGE_PENDING, null, null, null,0)
        {
        }

        public TiMessage(TiEvent e)
            : this(null,null, TiFoundation.EVENT_MESSAGE_PENDING, null, e, null,0)
        {
            // this.Event.AssignFrom(e);
        }

        public TiMessage(string msgtext)
            : this("*", null, TiFoundation.EVENT_MESSAGE_PENDING, msgtext, null, 0)
        {

        }

        /*
        public TiMessage(object o)
        {
            Type t = o.GetType();
            switch (t.Name)
            {
                case "string":
                    this("*", null, TiFoundation.EVENT_MESSAGE_PENDING, msgtext, null, 0);
                    msg = new TiMessage(o as string);
                    break;
                case "String":
                    msg = new TiMessage(o as string);
                    break;
                case "TiEvent":
                    msg = new TiMessage(o as TiEvent);
                    break;
                default:
                    msg = new TiMessage();
                    msg.Object = o;
                    break;
            }
        }
        */

        public TiMessage(string addrto, string msgtext)
            : this("*", null, TiFoundation.EVENT_MESSAGE_PENDING, msgtext, null, 0)
        {

        }

        public TiMessage(string addrfrom, string addrto, string msgtext)
            : this(null, null, TiFoundation.EVENT_MESSAGE_PENDING, msgtext, null, 0)
        {

        }

        public TiMessage(string addrfrom, string addrto, int msgid, string msgtext, TiEvent e=null, Object o=null, int intvalue=0)
        {
            this.AddrFrom = addrfrom;
            this.AddrTo = addrto;
            this.Id = msgid;
            this.Text = msgtext;
            // this.Event = new TiEvent();
            this.Object = o;
            this.Event = (e == null) ? null : e.Clone();
            /*
            if (e != null)
                this.Event.AssignFrom(e);
            else
                this.Event.Clear();
            */
            this.IntValue = intvalue;
        }

        public void AssignFrom(TiMessage from)
        {
            this.AddrFrom = from.AddrFrom;
            this.AddrTo = from.AddrTo;
            this.Id = from.Id;
            this.Text = from.Text;
            this.Event = (from.Event == null) ? null : from.Event.Clone();
            // this.Event = from.Event;
            this.Object = from.Object;
            /*
            if (this.Event != null)
                this.Event.AssignFrom(from.Event);
            else
                this.Event.Clear();
            */
            this.IntValue = from.IntValue;
        }

        public void Clear()
        {
            this.Id = TiFoundation.MSG_UNKNOWN;
            this.AddrFrom = null;
            this.AddrTo = null;
            this.Text = null;
            /*
            if (this.Event != null)
                this.Event.Clear();
            */
            this.Event = null;
            this.Object = null;
            this.IntValue = 0;
        }
    }
}
