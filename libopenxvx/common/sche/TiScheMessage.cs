﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Xvx.Foundation;

namespace App.Xvx.Common.Sche
{
    public class TiScheMessage
    {
        public int MsgId = 0;
        public int UserEventId = 0;
        public int TaskId = 0;
        public string Text = null;
        public int IntValue = 0;

        public TiScheMessage()
        {
            Clear();
        }

        public TiScheMessage(int msgid, int taskid, string msg, int percentage)
        {
            this.MsgId = msgid;
            this.UserEventId = 0;
            this.TaskId = taskid;
            this.Text = msg;
            this.IntValue = percentage;
        }

        public void Clear()
        {
            this.MsgId = TiFoundation.MSG_UNKNOWN;
            this.UserEventId = 0;
            this.TaskId = 0;
            this.Text = null;
            this.IntValue = 0;
        }
    }
}
