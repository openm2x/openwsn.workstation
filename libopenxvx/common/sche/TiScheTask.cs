﻿using App.Xvx.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using App.Xvx.Foundation;
using System.Diagnostics;
using App.Xvx.Common.MsgService;

namespace App.Xvx.Common.Sche
{
    // TODO: Plan to use this delegate to replace the queue now
    public delegate void IiScheTaskSendReportDelegate(TiEvent e, string message);

    public class TiScheTask : IiTask
    {
        /// <summary>
        /// Unique task identifier.
        /// </summary>
        private int _id;       

        private string _name;

        private string description;

        // private IiTask task;

        private int interval=100;

        private TiTaskState _state = TiTaskState.INIT;
        // private TiScheMessageQueue _msgqueue;
        private TiMsgQueue<TiScheMessage> _msgqueue;


        public int Id { get => _id; }
        // public IiTask Task { get => task; set => task = value; }
        public string Description { get => description; set => description = value; }
        public string Name { get => _name; }
        public TiMsgQueue<TiScheMessage> MsgQueue { get => _msgqueue; }
        public int Interval { get => interval; set => interval = value; }
        public int Type { get => type; set => type = value; }
        // public TiTaskState State { get => _state; set => _state = value; }
        public Object Input { get; set; }
        public Object Output { get; set; }
        public int ReturnValue { get; set; }  

        /// <summary>
        /// timer type
        /// 0-singal thread timer,only run task after the previous task end
        /// 1-general multi thread timer,run based on timer,be careful about interval and task run time
        /// </summary>
        private int type = 0;

        public TiScheTask(TiMsgQueue<TiScheMessage> msgqueue)
        {
            this._id = this.GetHashCode(); // task.GetHashCode();
            this._name = this.GetType().Name; // task.GetType().Name;
            this._state = TiTaskState.INIT;
            this._msgqueue = msgqueue;
            this.Interval = 1;
        }

        public virtual void Evolve(TiEvent e)
        {
            throw new NotImplementedException();
        }

        public virtual void Clear()
        {
            throw new NotImplementedException();
        }

        public TiTaskState State()
        {
            return _state;
        }

        public void SetState(TiTaskState state)
        {
            _state = state;
        }

        // public void SetScheMessageQueue(TiScheMessageQueue msgqueue)
        public void SetScheMessageQueue(TiMsgQueue<TiScheMessage> msgqueue)
        {
            this._msgqueue = msgqueue;
        }

        public void SendMessage(TiScheMessage msg)
        {
            Debug.Assert(msg.MsgId != TiFoundation.MSG_UNKNOWN);
            msg.TaskId = this.Id;
            _msgqueue.Push(msg);
        }

        public void ReportProgress(int percentage, string msgtext)
        {
            TiScheMessage msg = new TiScheMessage();

            msg.MsgId = TiFoundation.MSG_PROGRESS;
            msg.TaskId = this.Id;
            msg.Text = msgtext;
            msg.IntValue = percentage;

            // _msgqueue.Push(TiFoundation.MSG_PROGRESS, this.Id, msgtext, percentage);
            _msgqueue.Push(msg);
        }

        public void ReportComplete(int reason, string msgtext)
        {
            TiScheMessage msg = new TiScheMessage();

            msg.MsgId = TiFoundation.MSG_COMPLETED;
            msg.TaskId = this.Id;
            msg.Text = msgtext;
            msg.IntValue = reason;

            _msgqueue.Push(msg);
        }
    }
}
