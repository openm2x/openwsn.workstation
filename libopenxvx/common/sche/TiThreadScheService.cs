﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
// using System.Windows.Forms;
using App.Xvx.Common.MsgService;
using App.Xvx.Foundation;

namespace App.Xvx.Common.Sche
{
    // public delegate void IiOnTaskMessageReceivedDelegate(TiScheMessage msg);

    // https://blog.csdn.net/houwc/article/details/52459088

    // C#常用多线程方法
    // https://blog.csdn.net/dcrmg/article/details/53945839

    //
    public class TiThreadScheService
    {
        public const int MAX_TASK_COUNT = 5;

        public enum TiScheState { IDLE=0, RUNNING}

        protected TiScheTaskQueue _tasklist;
        protected Thread _worker;
        protected bool _threadstoprequest;
        protected IiEventListener _listener;
        protected int _sleepcount;
        protected int _progress;
        protected DateTime _lastdatetime = default(DateTime);
        protected TiScheState _state;

        protected TiMsgQueue<TiScheMessage> _msgqueue;
        protected TiScheMessage _currentmessage;
        public TiMsgQueue<TiScheMessage> MsgQueue { get => _msgqueue; }

        public TiThreadScheService(IiEventListener listener)
        {
            _tasklist = new TiScheTaskQueue(MAX_TASK_COUNT);
            _msgqueue = new TiMsgQueue<TiScheMessage>();

            ParameterizedThreadStart starter = new ParameterizedThreadStart(_DoWork);
            _worker = new Thread(starter);
            // _worker.WorkerReportsProgress = true;
            // _worker.WorkerSupportsCancellation = true;
            // _worker.DoWork += new DoWorkEventHandler(_DoWork);
            // _worker.ProgressChanged += new ProgressChangedEventHandler(_OnWorkerProgressChanged);
            // _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_OnRunWorkerCompleted);

            _worker.IsBackground = true;
            _threadstoprequest = false;

            _listener = listener;
            _sleepcount = 0;
            _progress = 0;
            _msgqueue.Clear();
            _currentmessage = new TiScheMessage();
            _state = TiScheState.IDLE;
        }

        public void SetEventListener(IiEventListener listener)
        {
            _listener = listener;
        }

        protected IiEventListener GetEventListener()
        {
            return _listener;
        }

        public int AddTask(TiScheTask task)
        {
            return _tasklist.Add(task);
        }

        // TODO
        public void RemoveTask(int taskid)
        {
            _tasklist.Remove(taskid);
        }

        public void Start()
        {
            if (_worker != null)
                _worker.Start();
        }

        public void Stop()
        {
            _threadstoprequest = true;

            // Wait for the main loop of the thread to check the "_threadstoprequest"
            // and exit elegantly by itself. The following "while" is used to wait
            // for the main thread execution body to exit.
            //
            while ((_worker.ThreadState != System.Threading.ThreadState.Stopped) 
                && (_worker.ThreadState != System.Threading.ThreadState.Aborted))
            {
                Thread.Sleep(10);
            }

            /*
            //调用Thread.Abort方法试图强制终止thread线程
            _worker.Abort();

            // 上面调用Thread.Abort方法后线程thread不一定马上就被终止了，所以我们在这里写了个
            // 循环来做检查，看线程thread是否已经真正停止。其实也可以在这里使用Thread.Join方法
            // 来等待线程thread终止，Thread.Join方法做的事情和我们在这里写的循环效果是一样的，
            // 都是阻塞主线程直到thread线程终止为止
            while (_worker.ThreadState != System.Threading.ThreadState.Aborted)
            {
                // 当调用Abort方法后，如果thread线程的状态不为Aborted，主线程就一直在这里做循环，
                // 直到thread线程的状态变为Aborted为止
                Thread.Sleep(100);
            }
            */

            /*
            if (_worker.WorkerSupportsCancellation == true)
            {
                // if (_worker.IsBusy)
                {
                    // Cancel the asynchronous operation.
                    _worker.CancelAsync();
                }
            }
            */
        }

        public void Clear()
        {
            if (_tasklist != null)
            {
                _tasklist.Clear();
            }
        }

        /// <summary>
        /// This method is the main execution body of the thread.  
        /// 
        /// attention: If the message is not empty, then this method will emit event to the 
        /// caller through the ReportProgress() method until the message is processed.
        /// So the caller may receive multiple same messages. Simply ignore them if received 
        /// more than once.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _DoWork(object o)
        {
            try
            {
                _DoWork2(null);
            }
            catch (Exception ex)
            {
                _OutputMessage(ex.Message);
            }
        }

        protected void _DoWork2(object init)
        {
            TiEvent ev = new TiEvent(0);
            TiScheTask task  ;
            TiScheMessage msg = new TiScheMessage();
            bool canceled = false;

            while (true)
            {
                // lock (_tasklist.Tasks)
                if (!_tasklist.IsEmpty())
                {
                    // Run all the tasks inside the scheduler
                    // foreach (var item in _tasklist)
                    foreach (var item in _tasklist.Index())
                    {
                        if (_threadstoprequest == true)
                        {
                            canceled = true;
                            break;
                        }

                        // Perform a time consuming operation and report progress.
                        // System.Threading.Thread.Sleep(500);
                        // worker.ReportProgress(i * 10);
                        //
                        task = _tasklist.Get(item); // (TiScheTask)item.Value;
                        if (task == null)
                            continue;

                        switch (task.State())
                        {
                            case TiTaskState.INIT:
                            case TiTaskState.IDLE:
                            case TiTaskState.READY:
                            case TiTaskState.RUNNING:
                                task.Evolve(ev);
                                break;

                            case TiTaskState.COMPLETED:
                                // Remove the task
                                // Attention not all tasks would like to set its own state. 
                                //
                                // TODO: trigger an event when a task done.
                                //
                                msg.Clear();
                                msg.MsgId = TiFoundation.MSG_COMPLETED;
                                msg.TaskId = 0;
                                msg.Text = "Scheduler stopped";
                                msg.IntValue = 100;
                                _msgqueue.Push(msg);
                                RemoveTask(task.Id);
                                break;

                            case TiTaskState.ERROR:
                            default:
                                msg.Clear();
                                msg.MsgId = TiFoundation.MSG_ERROR;
                                msg.TaskId = task.Id;
                                msg.Text = "Scheduler stopped";
                                msg.IntValue = 100;
                                _msgqueue.Push(msg);

                                RemoveTask(task.Id);
                                break;
                        }
                    } // end foreach
                }

                // Try stop the scheduler (not a specified task).
                if ((canceled == true) || (_threadstoprequest))
                {
                    _threadstoprequest = false;

                    msg.Clear();
                    msg.MsgId = TiFoundation.MSG_SCHEDULER_CANCELED;
                    msg.TaskId = 0;
                    msg.Text = "Scheduler stopped";
                    msg.IntValue = 100;
                    _msgqueue.Push(msg);

                    // Exit from the while and the DoWork() method come to it's ending.
                    break;
                }

                // Try to process the messages in the msg queue. These messages are generated
                // by the tasks during execution.
                //
                // In the DoWork() method, we only peek a message and then to indicate the 
                // master thread to further process them. Should NOT delete the messages
                // From the queue.
                //
                // Why we call worker.ReportProgress() here? Because we only use it to indicate
                // the invoker that some messages generated and need processing.

                // if (!_msgqueue.IsEmpty())
                // {
                //    worker.ReportProgress(50, "There is message pending in the queue.");
                // }

                Evolve(null);

                _sleepcount++;
                if (_sleepcount > 50)
                {
                    // Give up the CPU to other thread.
                    if (_tasklist.IsEmpty())
                        System.Threading.Thread.Sleep(100);
                    else
                        System.Threading.Thread.Sleep(10);
                    _sleepcount = 0;
                }

            } // end while
        }

        // Generate a event based on the message type. Then the listener's
        // owner object can be notified by the listener mechanism. 
        // You should can Read() of this class to process the message.
        // It's the Read() method to really delete the message from the queue.
        //
        protected void _OnTaskOutputMessageReceived(TiScheMessage msg)
        {
            // The listener cannot be null, because we require the listen handler to 
            // read the message out from the queue.
            //
            Debug.Assert(_listener != null);

            if (_listener == null)
            {
                // Simply consume the message in this case in order to avoid overflow
                // in the low level scheduler message queue.
                // TiScheMessage m= new TiScheMessage();
                // Read(out m);
                // _Read();
            }
            else
            {
                _currentmessage = msg;
                TiEvent ev = new TiEvent
                {
                    Id = Xvx.Foundation.TiFoundation.EVENT_MESSAGE_PENDING,
                    Source = this
                };
                _listener.Evolve(ev);
            }
        }

        /// <summary>
        /// Return the latest scheduler message item. The item will be remmoved from internal 
        /// queue at the same time.
        /// 
        /// This method should be called when the other modules received a event
        /// to indicate there's message pending. This method will delete the message
        /// in the queue.
        /// </summary>
        /// <returns></returns>
        public TiScheMessage Read()
        {
            // TiScheMessage msg = _currentmessage;
            // _currentmessage = null;

            TiMsgQueueItem<TiScheMessage> item = new TiMsgQueueItem<TiScheMessage>();
            if (_msgqueue.TryDequeue(out item))
                return item.Value;
            else
                return null;
        }

        /// <summary>
        /// Evolve the state machine of this class. Mainly used to process
        /// external event input.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected int Evolve(TiEvent e)
        {
            if (_listener != null)
            {
                _listener.Evolve(e);
            }

            /*
            if ((_listener != null) && (!_msgqueue.IsEmpty()))
            {
                e = new TiEvent(TiFoundation.EVENT_SCHE_REPORT, this, null, null);
                _listener.Evolve(e);
            }
            */
                       
            /*
            bool retvalue = true;
            TiScheMessage msg = new TiScheMessage();
            TiEvent ev = new TiEvent();

            Debug.Assert(_listener != null);

            while (retvalue)
            {
                // retvalue = _msgstream.TryDequeue(out msg);
                retvalue = _msgqueue.TryPeek(out msg);
                if (!retvalue)
                {
                    OnTaskOutputMessageReceived(msg);
                }
            }
            */
            return 0;
        }

        // Old source code


        private void _OutputMessage(string s)
        {
            // MessageBox.Show(s, "TiScheService");
        }


    }
}
