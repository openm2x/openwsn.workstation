﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Xvx.Common.Sche;

namespace App.Xvx.Common.Sche
{
    /// <summary>
    /// Task queue for scheduler. This queue supports multi-thread operations.
    /// 
    /// modified by zhangwei on 2018.10.25
    /// attention: Though I added locks on almost all the method in this class,
    /// it still cannot avoid all conflictions. For example:  You access the Count()
    /// property and then delete one item in parallel.
    /// 
    /// Reference
    /// ConcurrentDictionary 对决 Dictionary+Locking,
    /// http://www.cnblogs.com/gaochundong/p/concurrent_dictionary_vs_dictionary_plus_locking.html
    /// 
    /// </summary>
    public class TiScheTaskQueue
    {
        protected Dictionary<int, TiScheTask> _tasklist = null;
        protected int _capacity;
        protected int _sequence = 1;

        public TiScheTaskQueue(int capacity)
        {
            _capacity = capacity;
            _tasklist = new Dictionary<int, TiScheTask>(capacity);
        }

        public Dictionary<int, TiScheTask> Tasks { get => _tasklist; set => _tasklist = value; }

        public int Add(TiScheTask task)
        {
            lock (_tasklist)
            {
                if (!IsFull())
                {
                    // task.SetScheMessageQueue(_msgqueue);
                    _tasklist[task.Id] = task;
                    return task.Id;
                }
                else
                    return -1;
            }
        }

        public void Remove(int taskid)
        {
            lock (_tasklist)
            {
                if (_tasklist.ContainsKey(taskid))
                {
                    _tasklist.Remove(taskid);
                }
            }
        }

        public void Clear()
        {
            lock (_tasklist)
            {
                _tasklist.Clear();
            }
        }

        public bool IsFull()
        {
            bool retvalue = false;
            lock (_tasklist)
            {
                retvalue = (_capacity <= _tasklist.Count);
            }
            return retvalue;
        }

        public bool IsEmpty()
        {
            bool retvalue = false;
            lock (_tasklist)
            {
                retvalue = (_tasklist.Count == 0);
            }
            return retvalue;
        }

        public int Count()
        {
            return _tasklist.Count;
        }

        public List<int> Index()
        {
            lock (_tasklist)
            {
                return new List<int>(_tasklist.Keys);
            }
        }

        public List<TiScheTask> Find()
        {
            lock (_tasklist)
            {
                return new List<TiScheTask>(_tasklist.Values);
            }
        }

        public TiScheTask Get(int id)
        {
            lock (_tasklist)
            {
                if (_tasklist.ContainsKey(id))
                    return _tasklist[id];
                else
                    return null;
            }
        }



        /*
        public List<TiScheTask> TaskList()
        {
            lock (_tasklist)
            {
                return _tasklist.Values.ToList();
            }
        }
        */

    }
}
