﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace App.Xvx.Common
{
    /// <summary>
    /// Helper class to manipulate object meta data.
    /// 
    /// History
    /// modified by zhangwei on 2020.02.13 
    /// - Add parameter "StringComparer.OrdinalIgnoreCase" to intialize a lot of 
    ///   creations of the dictionary<string,string>. This will help to ease the
    ///   searching of keys.
    /// 
    /// </summary>
    public class TiMetaObjectService
    {
        public TiMetaObjectService()
        {

        }

        // public static T CreateInstance<T>() where T : new()
        public static T CreateInstance<T>()
        {
            //用Activator .CreateInstance创建函数实例,默认的不带参数的构造函数
            // IObjcet obj = (IObjcet)Activator.CreateInstance(System.Type.GetType("ActivatorCreateInstance.ClassExam,ActivatorExample"), null);
            // obj.printName();
            var o = Activator.CreateInstance(typeof(T), null);
            //System.Type .GetType  命名空间.类名,程序集
            return (T)o;
        }

        public static T CreateInstance<T>(System.Type type) where T : new()
        {
            //用Activator .CreateInstance创建函数实例,默认的不带参数的构造函数
            // IObjcet obj = (IObjcet)Activator.CreateInstance(System.Type.GetType("ActivatorCreateInstance.ClassExam,ActivatorExample"), null);
            // obj.printName();
            var o = Activator.CreateInstance(type);
            //System.Type .GetType  命名空间.类名,程序集
            return (T)o;
        }

        public static T CreateInstance<T>(string type) where T : new()
        {
            //用Activator .CreateInstance创建函数实例,默认的不带参数的构造函数
            // IObjcet obj = (IObjcet)Activator.CreateInstance(System.Type.GetType("ActivatorCreateInstance.ClassExam,ActivatorExample"), null);
            // obj.printName();
            var o = Activator.CreateInstance(System.Type.GetType(type), null);
            //System.Type .GetType  命名空间.类名,程序集
            return (T)o;
        }

        public static T CreateInstance<T>(string type, params object[] args) where T : new()
        {
            //调用ClassExam类的另一个带参数构造函数
            // IObjcet obj = (IObjcet)System.Activator.CreateInstance(System.Type.GetType("ActivatorCreateInstance.ClassExam,ActivatorExample"), new string[] { "seted new name" });
            // obj.printName();
            var o = Activator.CreateInstance(System.Type.GetType(type), args);
            //System.Type .GetType  命名空间.类名,程序集
            return (T)o;
        }

        public static bool IsValueType<T>()
        {
            return typeof(T).IsValueType;
        }
        public static bool IsValueType(object obj)
        {
            return obj != null && obj.GetType().IsValueType;
        }


        /// <summary>  
        ///   
        /// 将对象属性转换为key-value对  
        /// </summary>  
        /// <param name="o"></param>  
        /// <returns></returns>  
        public static Dictionary<String, Object> ToMap(Object o)
        {
            Dictionary<String, Object> map = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            Type t = o.GetType();
            PropertyInfo[] pi = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo p in pi)
            {
                // All the property should have a Get method, that's why we call GetGetMethod here.
                MethodInfo mi = p.GetGetMethod();

                if (mi != null && mi.IsPublic)
                {
                    map.Add(p.Name, mi.Invoke(o, new Object[] { }));
                }
            }

            return map;
        }

        public static Dictionary<String, Object> GetModifiedPropetyAndValues(Object current, Object origin)
        {
            Dictionary<String, Object> currentmap = ToMap(current);

            if (origin == null)
            {
                return currentmap;
            }
            else
            {
                Dictionary<String, Object> originmap = ToMap(origin);

                object v1, v2;
                Dictionary<String, Object> result = new Dictionary<String, Object>(StringComparer.OrdinalIgnoreCase);
                foreach (KeyValuePair<string, Object> item in currentmap)
                {
                    v1 = currentmap[item.Key];
                    if (originmap.TryGetValue(item.Key, out v2))
                    {
                        if (v1 == null)
                        {
                            if (v2 != null)
                                result.Add(item.Key, null);
                        }
                        else
                        {
                            if (!v1.Equals(v2))
                            {
                                result.Add(item.Key, v1);
                            }
                        }
                    }
                    else
                    {
                        result.Add(item.Key, item.Value);
                    }
                }
                return result;
            }
        }

        // 判断两个object是否相同
        // https://blog.csdn.net/lrxin/article/details/18882093
        protected static bool Equal(object v1, object v2)
        {
            bool flag = false;
            Type t = v1.GetType();
            System.Reflection.PropertyInfo[] pros = t.GetProperties();
            foreach (System.Reflection.PropertyInfo pro in pros)
            {
                if (pro.Name == "ID")
                {
                    continue;
                }//判断值是否相同
                object a = pro.GetValue(v1, null);
                object b = pro.GetValue(v2, null);
                flag = ((a == null && b == null) || ((a != null && b != null) && (a.Equals(b))));
                if (!flag)
                {
                    break;
                }
            }
            return flag;
        }

        /// <summary>
        /// 字典类型转化为对象
        /// https://blog.csdn.net/Shiyaru1314/article/details/52918351?locationNum=9&fps=1
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static T DicToObject<T>(Dictionary<string, object> dic) where T : new()
        {
            var md = new T();
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            foreach (var d in dic)
            {
                var filed = textInfo.ToTitleCase(d.Key);
                try
                {
                    var value = d.Value;
                    md.GetType().GetProperty(filed).SetValue(md, value);
                }
                catch (Exception)
                {

                }
            }
            return md;
        }


        // another method is to use serialization
        public static void DuplicateList<T>(List<T> from, List<T> to)
        {
            if (typeof(T).IsValueType)
            {
                foreach (T o in from)
                {
                    to.Add(o);
                }
            }
            else
            {
                T tmp = default(T);
                ICloneable c = tmp as ICloneable;
                if (c == null)
                {
                    // The object stored in the list doesn't support the ICloneable interface
                    foreach (T o in from)
                    {
                        to.Add(o);
                    }
                }
                else
                {
                    foreach (T o in from)
                    {
                        c = o as ICloneable;
                        to.Add((T)c);
                    }
                }
            }
        }

        /// <summary>
        /// Perform a deep Copy of the object.
        /// </summary>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        public static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            using (MemoryStream objectStream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(objectStream, source);
                objectStream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(objectStream);
            }
        }

        public static PropertyInfo[] GetPropertyInfos(Type type)
        {
            return type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
        }

        /// <summary>
        /// 利用反射给不同类型对象同名属性赋值
        /// https://www.cnblogs.com/sxypeace/p/5888345.html
        /// 
        /// Class1 objfrom,Class2 objto;
        /// ObjectReflection.AutoMapping<Class1, Class2>(objfrom, objto);
        /// 将 objfrom 的属性复制给objto的同名属性。
        /// 
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="s"></param>
        /// <param name="t"></param>
        public static void AutoMapping<S, T>(S s, T t)
        {
            // get source PropertyInfos
            PropertyInfo[] pps = GetPropertyInfos(s.GetType());
            // get target type
            Type target = t.GetType();

            foreach (var pp in pps)
            {
                PropertyInfo targetPP = target.GetProperty(pp.Name);
                object value = pp.GetValue(s, null);

                if (targetPP != null && value != null)
                {
                    targetPP.SetValue(t, value, null);
                }
            }
        }

        /// <summary>
        /// 利用反射实现两个类的对象之间相同属性的值的复制.
        /// 还可以利用字段的访问属性，字段的类型，命名等在进行进一步的删选，
        /// 我们只需要改变GetProperties()函数和if的判断条件，这样我们就可以进行进一步的筛选。
        /// 这个的原理就是利用反射获得两个对象的所有属性字段，然后根据相同的名字进行复制，
        /// 写法可以有很多种的写法，可以根据不同的属性字段命名的方式来改写。
        /// https://blog.csdn.net/u013093547/article/details/53584591
        /// </summary>
        /// <typeparam name="D"></typeparam>
        /// <typeparam name="S"></typeparam>
        /// <param name="s"></param>
        /// <returns></returns>
        public static D Mapper<D, S>(S s)
        {
            D d = Activator.CreateInstance<D>();
            try
            {
                var Types = s.GetType();//获得类型
                var Typed = typeof(D);
                foreach (PropertyInfo sp in Types.GetProperties())//获得类型的属性字段
                {
                    foreach (PropertyInfo dp in Typed.GetProperties())
                    {
                        if (dp.Name == sp.Name)//判断属性名是否相同
                        {
                            dp.SetValue(d, sp.GetValue(s, null), null);//获得s对象属性的值复制给d对象的属性
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return d;
        }

        //这个类对可空类型进行判断转换，要不然会报错
        private static object _HackType(object value, Type conversionType)
        {
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                    return null;

                System.ComponentModel.NullableConverter nullableConverter = new System.ComponentModel.NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }
            return Convert.ChangeType(value, conversionType);
        }

        private static bool IsNullOrDBNull(object obj)
        {
            return ((obj is DBNull) || string.IsNullOrEmpty(obj.ToString())) ? true : false;
        }

        // TODO
        // public static IDictionary<string, T> ToDictionary<T>(object source)
        public static Dictionary<string, T> ToDictionary<T>(object source)
        {
            if (source == null)
                ThrowExceptionWhenSourceArgumentIsNull();

            var dictionary = new Dictionary<string, T>(StringComparer.OrdinalIgnoreCase);
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source))
                _AddPropertyToDictionary<T>(property, source, dictionary);
            return dictionary;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public string Dump(object o, int indent=0, bool recursive=false)
        {
            StringBuilder sb = new StringBuilder();
            Dump(o, sb, indent, recursive);
            return sb.ToString();
        }

        public static void Dump(object o, StringBuilder sb, int indent=0, bool recursive=false)
        {
            // Dictionary<string, object> map = ToDictionary<object>(o);
            // var dictionary = new Dictionary<string, T>(StringComparer.OrdinalIgnoreCase);
            // foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source))
            //    _AddPropertyToDictionary<T>(property, source, dictionary);

            if (o == null)
            {
                sb.Append("(value is null)");
            }
            else if (IsValueType(o))
            {
                sb.Append(o.ToString());
            }
            else if (o is Array)
            {
                Dump(o as Array, sb, indent, recursive, 100);
            }
            /*
            else if ((o as IDictionary) != null)
            {
                foreach (var item in o)
                {
                    _InsertTableSpace(sb, indent);
                    string tmp = (item.Value != null) ? item.Value.ToString() : "(null)";
                    sb.AppendFormat("({0},{1})\r\n", item.Key.ToString(), tmp);
                }
            }
            */
            else
            {
                _InsertTableSpace(sb, indent);
                var dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(o))
                {
                    try
                    {
                        sb.Append("\r\n");
                        _InsertTableSpace(sb, indent);
                        object value = property.GetValue(o);
                        if (value == null)
                            dictionary.Add(property.Name, "(value is null)");
                        else if (IsValueType(value))
                            dictionary.Add(property.Name, value.ToString());
                        else
                        {
                            if (recursive)
                                Dump(value, sb, indent + 1, recursive);
                            else
                                dictionary.Add(property.Name, value.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        dictionary.Add(property.Name, "(value is null)");
                        sb.Append(ex.Message);
                    }
                }
                /*
                _InsertTableSpace(sb, indent);
                string tmp;

                foreach (var item in dictionary)
                {
                    try
                    {
                        tmp = (item.Value != null) ? item.Value.ToString() : "(null)";
                        _InsertTableSpace(sb, indent);
                        sb.AppendFormat("({0},{1})\r\n", item.Key.ToString(), tmp);
                    }
                    catch
                    {
                        _InsertTableSpace(sb, indent);
                        sb.AppendFormat("({0},{1})\r\n", item.Key.ToString(), "(expcetion occured)");
                    }
                }
                */
            }
        }

        public static void Dump(Dictionary<string, object> o, StringBuilder sb, int indent = 0, bool recursive = false)
        {
            if (o == null)
            {
                sb.Append("(value is null)");
            }
            else {
                sb.Append("\r\n");
                _InsertTableSpace(sb, indent);
                sb.Append('{');
                sb.Append("\r\n");
                foreach (var item in o)
                {
                    _InsertTableSpace(sb, indent+1);
                    string tmp = (item.Value != null) ? item.Value.ToString() : "(null)";
                    sb.AppendFormat("({0},{1})\r\n", item.Key.ToString(), tmp);
                }
                sb.Append("\r\n");
                _InsertTableSpace(sb, indent);
                sb.Append('}');
            }
        }

        public static void Dump(Array o, StringBuilder sb, int indent = 0, bool recursive = false, int maxcount = 50)
        {
            if (o == null)
            {
                sb.Append("(value is null)");
            }
            else
            {
                int count = 0;
                sb.Append("[");
                foreach (var item in o)
                {
                    if (count > maxcount)
                        break;
                    string tmp = (item == null) ? "(null)" : item.ToString();
                    sb.AppendFormat("{0}, ", tmp);
                    count++;
                }
                sb.Append("]");
            }
        }

        public static void Dump(List<object> o, StringBuilder sb, int indent = 0, bool recursive = false, int maxcount = 50)
        {
            if (o == null)
            {
                sb.Append("(value is null)");
            }
            else
            {
                int count = 0;
                sb.Append("{");
                foreach (var item in o)
                {
                    if (count > maxcount)
                        break;
                    string tmp = (item == null) ? "(null)" : item.ToString();
                    sb.AppendFormat("{0}, ", tmp);
                    count++;
                }
                sb.Append("}");
            }
        }

        private static void _InsertTableSpace(StringBuilder buf, int count)
        {
            for (int i = 0; i < count; i++)
                buf.Append("");
        }

        private static void _AddPropertyToDictionary<T>(PropertyDescriptor property, object source, Dictionary<string, T> dictionary)
        {
            object value = property.GetValue(source);
            if (IsOfType<T>(value))
                dictionary.Add(property.Name, (T)value);
        }

        private static bool IsOfType<T>(object value)
        {
            return value is T;
        }

        private static void ThrowExceptionWhenSourceArgumentIsNull()
        {
            throw new ArgumentNullException("source", "Unable to convert object to a dictionary. The source object is null.");
        }
    }
}
