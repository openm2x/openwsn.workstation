﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

// C# 对象、文件与二进制串（byte数组）之间的转换
// https://www.cnblogs.com/Arlar/p/5860188.html
// https://my.oschina.net/Tsybius2014/blog/352409#navbar-header
// （很实用）

namespace App.WorkStation.Common
{
    class ByteConvertor
    {
        /// <summary>
        /// 工具类：对象与二进制流间的转换
        /// </summary>
        class ByteConvertHelper
        {
            /// <summary>
            /// 将对象转换为byte数组。
            /// 
            /// 需要注意的是，用这个方式进行结构与byte数组间的转换，结构或类必须有Serializable特性。
            /// 否则会有异常（SerializationException）：“程序集 "XXX, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" 中的类型 "XXX.XXX" 未标记为可序列化”
            /// </summary>
            /// <param name="obj">被转换对象</param>
            /// <returns>转换后byte数组</returns>
            public static byte[] Object2Bytes(object obj)
            {
                byte[] buff;
                using (MemoryStream ms = new MemoryStream())
                {
                    IFormatter iFormatter = new BinaryFormatter();
                    iFormatter.Serialize(ms, obj);
                    buff = ms.GetBuffer();
                }
                return buff;
            }

            /// <summary>
            /// 将byte数组转换成对象
            /// </summary>
            /// <param name="buff">被转换byte数组</param>
            /// <returns>转换完成后的对象</returns>
            public static object Bytes2Object(byte[] buff)
            {
                object obj;
                using (MemoryStream ms = new MemoryStream(buff))
                {
                    IFormatter iFormatter = new BinaryFormatter();
                    obj = iFormatter.Deserialize(ms);
                }
                return obj;
            }

            /// <summary>
            /// 测试结构
            /// </summary>
            [Serializable]
            struct TestStructure
            {
                public string A; //变量A
                public char B;   //变量B
                public int C;    //变量C

                /// <summary>
                /// 构造函数
                /// </summary>
                /// <param name="paraA"></param>
                /// <param name="paraB"></param>
                /// <param name="paraC"></param>
                public TestStructure(string paraA, char paraB, int paraC)
                {
                    this.A = paraA;
                    this.B = paraB;
                    this.C = paraC;
                }

                /// <summary>
                /// 输出本结构中内容
                /// </summary>
                /// <returns></returns>
                public string DisplayInfo()
                {
                    return string.Format("A:{0};B:{1};C:{2}", this.A, this.B, this.C);
                }
            }

            static void Test(string[] args)
            {
                TestStructure tsA = new TestStructure("1234", '5', 6);
                byte[] bytTemp = ByteConvertHelper.Object2Bytes(tsA);
                Console.WriteLine("数组长度：" + bytTemp.Length);
                TestStructure tsB = (TestStructure)ByteConvertHelper.Bytes2Object(bytTemp);
                Console.WriteLine(tsB.DisplayInfo());

                Console.ReadLine();
            }
        }
    }
}
