﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace App.WorkStation.Common
{
    class MarshalByteConvertor
    {
    }

    /// <summary>
    /// 工具类：对象与二进制流间的转换
    /// 转换的byte[]数组短了非常多，更加节省空间. 比serialization方法要节省。
    /// </summary>
    class ByteConvertHelper
    {
        /// <summary>
        /// 将对象转换为byte数组
        /// </summary>
        /// <param name="obj">被转换对象</param>
        /// <returns>转换后byte数组</returns>
        public static byte[] Object2Bytes(object obj)
        {
            byte[] buff = new byte[Marshal.SizeOf(obj)];
            IntPtr ptr = Marshal.UnsafeAddrOfPinnedArrayElement(buff, 0);
            Marshal.StructureToPtr(obj, ptr, true);
            return buff;
        }

        /// <summary>
        /// 将byte数组转换成对象
        /// </summary>
        /// <param name="buff">被转换byte数组</param>
        /// <param name="typ">转换成的类名</param>
        /// <returns>转换完成后的对象</returns>
        public static object Bytes2Object(byte[] buff, Type typ)
        {
            IntPtr ptr = Marshal.UnsafeAddrOfPinnedArrayElement(buff, 0);
            return Marshal.PtrToStructure(ptr, typ);
        }
    }

    /// <summary>
    /// 测试结构
    /// </summary>
    struct TestStructure
    {
        public string A; //变量A
        public char B;   //变量B
        public int C;    //变量C

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="paraA"></param>
        /// <param name="paraB"></param>
        /// <param name="paraC"></param>
        public TestStructure(string paraA, char paraB, int paraC)
        {
            this.A = paraA;
            this.B = paraB;
            this.C = paraC;
        }

        /// <summary>
        /// 输出本结构中内容
        /// </summary>
        /// <returns></returns>
        public string DisplayInfo()
        {
            return string.Format("A:{0};B:{1};C:{2}", this.A, this.B, this.C);
        }
        static void Test(string[] args)
        {
            TestStructure tsA = new TestStructure("1234", '5', 6);
            byte[] bytTemp = ByteConvertHelper.Object2Bytes(tsA);
            Console.WriteLine("数组长度：" + bytTemp.Length);
            TestStructure tsB = (TestStructure)ByteConvertHelper.Bytes2Object(
                bytTemp, Type.GetType("ByteConverter2.TestStructure"));
            Console.WriteLine(tsB.DisplayInfo());

            Console.ReadLine();
        }
    }

}
