﻿using App.Xvx.Common.MsgService;
using App.Xvx.Common.Sche;
using App.Xvx.Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WorkStation.Service
{
    public class TiAppMonitorTask : TiScheTask
    {
        private IiEventListener _notifylistener;

        public TiAppMonitorTask(IiEventListener notifylistener, TiMsgQueue<TiScheMessage> queue)
            : base(queue)
        {
            _notifylistener = notifylistener;
        }

        public override void Evolve(TiEvent e)
        {
            /*
            if (!base.MsgQueue.IsEmpty())
            {
                TiEvent e2 = new TiEvent(TiFoundation.EVENT_SCHE_REPORT, "sche msg queue has pending messages for processing");
                _notifylistener.Evolve(e2);
            }
            */
            _notifylistener.Evolve(e);
        }
    }
}
