﻿using System.Collections.Concurrent;
using App.Xvx.Foundation;

namespace App.WorkStation.Service
{
    /// <summary>
    /// This module implements a data management cache for the application.
    /// It can buffer some data in the memory through queue. Furthermore, this cache solves 
    /// the concurrent problem.
    /// 
    /// modified by zhangwei on 2018.09.09
    /// - Upgrade the Peek() method. The behavior from Blocking mode to Non-blocking mode.
    /// 
    /// </summary>
    public class TiAppCacheService
    {
        private ConcurrentQueue<int> _queue = new ConcurrentQueue<int>();
        private bool _modified = false;
        private bool _isdatadirty = false;

        public TiAppCacheService()
        {
        }

        public bool Modified { get => _modified; set => _modified = value; }

        public void Push(int value)
        {
            lock (_queue)
            {
                _queue.Enqueue(value);
                _modified = true;
            }
        }

        /// <summary>
        /// return the first item but not remove.
        /// 
        /// </summary>
        /// <returns></returns>
        public int? Peek()
        {
            /*
            int buf = null;
            if (_queue.Count > 0)
            {
                bool res = false;
                while (!res)
                {
                    res = _queue.TryPeek(out buf);
                }
            }
            return buf;
            */
            int value;
            bool retvalue = _queue.TryPeek(out value);
            return (retvalue) ? (int?)value : null;
        }

        public bool TryPeek(out int value)
        {
            bool retvalue;
            lock (_queue)
            {
                retvalue = _queue.TryPeek(out value);
            }
            return retvalue;
        }

        /// <summary>
        /// remove the first item
        /// </summary>
        public void Pop()
        {
            lock (_queue)
            {
                if (_queue.Count > 0)
                {
                    /*
                    int buf = new int(4096);
                    bool res = false;
                    while (!res)
                    {
                        res = _queue.TryDequeue(out buf);
                    }
                    */
                    int value;
                    _queue.TryDequeue(out value);
                }
            }
        }

        public bool TryPop(out int value)
        {
            return _queue.TryDequeue(out value);
        }

        public bool IsEmpty()
        {
            return _queue.IsEmpty;
        }

        public bool IsDataDirty { get => _isdatadirty; set => _isdatadirty = value; }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
