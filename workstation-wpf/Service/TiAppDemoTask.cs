﻿using System;
using System.Diagnostics;
//using System.Windows.Forms;
using App.Xvx.Common;
using App.Xvx.Common.MsgService;
using App.Xvx.Common.Sche;
using App.Xvx.Foundation;
using App.WorkStation.Service;

namespace App.WorkStation.Service
{
    public struct TiAppDemoTaskInit
    {
        public string rawdatafile;
        public string drivername;
    }

    /// <summary>
    /// Task: This demo task is to show how to develop a task which can be scheduled 
    /// by the scheduler service. The demo task simulates the behavior of the I/O 
    /// data reading behavior and it will put the random data into the queue.
    /// 
    /// modified by zhangwei on 2020.06.26
    /// - Rename method Execute() to Evolve() and add a parameter TiEvent.
    /// </summary>
    class TiAppDemoTask : TiScheTask, IiTask
    {
        private TiAppService _svc;
        private short _emptycount = 0;
        private long _totalcount = 0;
        private int _evolecount = 0;
        private TiAppDemoTaskInit _init;
        private Random _rand = new Random();
        private DateTime _lasttime;

        public TiAppDemoTask(TiAppService service, TiMsgQueue<TiScheMessage> queue)
            : base(queue)
        {
            this._svc = service;
            this._totalcount = 0;
            this._evolecount = 0;
            Debug.Assert(service.Xio != null); 
        }

        public void Init(TiAppDemoTaskInit init)
        {
            _init = init;
            SetState(TiTaskState.RUNNING);
            _lasttime = DateTime.Now;

            // TODO: assume XioService is active. (already called uio.Scan() and uio.Open())
            Debug.Assert(_svc.Xio != null);
        }

        public override void Evolve(TiEvent e)
        {
            bool done = false;

            do
            {
                switch (State())
                {
                    case TiTaskState.INIT:
                    case TiTaskState.IDLE:
                    case TiTaskState.READY:
                        _lasttime = DateTime.Now;
                        SetState(TiTaskState.RUNNING);
                        break;

                    case TiTaskState.RUNNING:
                        DateTime now = DateTime.Now;
                        TimeSpan interval = now.Subtract(_lasttime);
                        if (interval.TotalMilliseconds > 500)
                        {
                            _evolecount++;
                            int value = _rand.Next(1, 101);
                            _svc.Cache.Push(value);
                            _lasttime = now;
                        }
                        done = true;
                        break;

                    case TiTaskState.COMPLETED:
                        _OnTaskCompleted();
                        done = true;
                        break;

                    // Attention the source code is called repeatedly, so the source code in 
                    // ERROR state will be called multiple times. That's why we add the Sleep()
                    // call here to delay the thread and output.
                    //
                    case TiTaskState.ERROR:
                    default:
                        Trace.WriteLine("TiAppDemoTask: Encounter error and came into Error statet.");
                        _OnTaskCompleted();
                        System.Threading.Thread.Sleep(100);
                        done = true;
                        break;
                } // switch

            } while (!done);

            // Output the progress information.
            // _evolecount++;
            if (_evolecount >= 10)
            {
                _evolecount = 0;
                string s = "TiAppDemoTask: generated random numbers for about 10 times.";
                Trace.WriteLine(s);
            }

            return;
        }

        public override void Clear()
        {

        }

        // This method will be called when the input come to its end.
        // Attention this is only a temporary design. 
        // Because some input may haven't the stream end.
        //
        private void _OnTaskCompleted()
        {
            // You can push a message into the msgqueue or set some flag to indicate
            // the end of the task.
        }
    }
}
