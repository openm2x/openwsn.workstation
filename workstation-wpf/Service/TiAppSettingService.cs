﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Xvx.Common;

namespace App.WorkStation.Service
{
    public class TiAppSettingService 
    {
        Dictionary<string, Object> _keyvalues;

        public TiAppSettingService(string filename) 
        {
            LoadDefaultSettings();
        }

        public void LoadDefaultSettings()
        {
            this._keyvalues = new Dictionary<string, Object>
            {
                { "AppName", "ioservice" },
                { "AppRootDir", "." },
                { "AppDataDir", "data" },
                { "AppTempDir", "temp" }
            };

        }
    }
}
