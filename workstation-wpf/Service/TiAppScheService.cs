﻿using App.WorkStation.Service;
using App.Xvx.Common.MsgService;
using App.Xvx.Common.Sche;
using App.Xvx.Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WorkStation.Service
{
    /// <summary>
    /// The scheduler is essentially a independent thread which can run separately from
    /// the main thread. It's a background type thread in the application. 
    /// 
    /// Why i call it "scheduler" because i don't want to create too many threads. 
    /// The policy is to use a scheduler thread to manage all standalone tasks.
    /// 
    /// </summary>
    public class TiAppScheService : TiThreadScheService
    {
        IiEventListener _listener;

        public TiAppScheService(IiEventListener listener) : base(listener)
        {
            _listener = listener;
        }

        public void Start(TiAppService avc)
        {
            var demotask = new TiAppDemoTask(avc, base._msgqueue);
            // var recvtask = new TiAppIoRecvTask(avc, base._msgqueue);
            // var savetask = new TiAppIoSaveTask(avc, null, base._msgqueue);
            var monitortask = new TiAppMonitorTask(_listener, base._msgqueue);

            AddTask(demotask);
            // AddTask(recvtask);
            // AddTask(savetask);
            // AddTask(monitortask);
            base.Start();
        }
    }
}
