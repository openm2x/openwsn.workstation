﻿using System.Collections.Concurrent;
using App.Xvx.Foundation;

namespace App.WorkStation.Service
{
    /// <summary>
    /// This module implements a data management cache for the application.
    /// It can buffer some data in the memory through queue. Furthermore, this cache solves 
    /// the concurrent problem.
    /// 
    /// modified by zhangwei on 2018.09.09
    /// - Upgrade the Peek() method. The behavior from Blocking mode to Non-blocking mode.
    /// 
    /// </summary>
    public class TiAppCacheServiceV2
    {
        private ConcurrentQueue<TiIoBuf> _queue = new ConcurrentQueue<TiIoBuf>();

        public TiAppCacheServiceV2()
        {
        }

        public void Push(TiIoBuf buf)
        {
            lock (_queue)
            {
                _queue.Enqueue(buf);
            }
        }

        /// <summary>
        /// return the first item but not remove.
        /// 
        /// </summary>
        /// <returns></returns>
        public TiIoBuf Peek()
        {
            /*
            TiIoBuf buf = null;
            if (_queue.Count > 0)
            {
                bool res = false;
                while (!res)
                {
                    res = _queue.TryPeek(out buf);
                }
            }
            return buf;
            */
            TiIoBuf buf = new TiIoBuf(4096);
            bool retvalue = _queue.TryPeek(out buf);
            return (retvalue) ? buf : null;
        }

        public bool TryPeek(out TiIoBuf buf)
        {
            bool retvalue;
            lock (_queue)
            {
                retvalue = _queue.TryPeek(out buf);
            }
            return retvalue;
        }

        /// <summary>
        /// remove the first item
        /// </summary>
        public void Pop()
        {
            lock (_queue)
            {
                if (_queue.Count > 0)
                {
                    /*
                    TiIoBuf buf = new TiIoBuf(4096);
                    bool res = false;
                    while (!res)
                    {
                        res = _queue.TryDequeue(out buf);
                    }
                    */
                    TiIoBuf buf = new TiIoBuf(4096);
                    _queue.TryDequeue(out buf);
                }
            }
        }

        public bool TryPop(out TiIoBuf buf)
        {
            return _queue.TryDequeue(out buf);
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
