﻿using App.Xvx.Common.MsgService;
using App.Xvx.Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace App.WorkStation.Service
{
    public class TiAppIoService
    {
        private TiAppIoService _io;
        private TiAppDataService _data;
        private string _appdatadir;

        private TiAppCacheService _cache;
        private IiEventListener _listener;
        // private IiMsgRouterService _msgservice;

        public TiAppIoService()
        {

        }

        public int Sum(int x1, int x2)
        {
            return _sum(x1, x2);
        }

        //[DllImport("ioservice.dll", EntryPoint= "dll_api_test01", CallingConvention=CallingConvention.StdCall)]
        [DllImport("ioservice.dll", EntryPoint = "dll_api_test01")]
        public static extern int _sum(int x1, int x2);

        [DllImport("ioservice.dll")]
        public static extern void _send(IntPtr msg, int len);

        public void SendTest01(IntPtr msg, int len)
        {
            const string reqString = @"test";
            var bytes = Encoding.Default.GetBytes(reqString);
            var p = Marshal.AllocHGlobal(Marshal.SizeOf(bytes));
            Marshal.Copy(bytes, 0, p, bytes.Length);
            _send( p, bytes.Length);
            Marshal.FreeHGlobal(p);
        }

        public void SendTest02(IntPtr msg, int len)
        {
            const string reqString = @"test";
            IntPtr intPtr = Marshal.StringToHGlobalAnsi(reqString);
            _send(intPtr, Encoding.Default.GetByteCount(reqString));
            Marshal.FreeHGlobal(intPtr);
        }
    }
}
