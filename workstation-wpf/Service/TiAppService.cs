﻿using App.Xvx.Common.MsgService;
using App.Xvx.Common.Sche;
using App.Xvx.Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WorkStation.Service
{
    /// <summary>
    /// Main Application Service
    /// 
    /// Reference
    /// c# 几种singleton 实现,
    /// https://www.cnblogs.com/scotth/p/10450243.html
    /// https://www.manning.com/books/c-sharp-in-depth-fourth-edition
    /// http://csharpindepth.com/Articles/General/Singleton.aspx#introduction
    /// 
    /// History
    /// author zhangwei on 2020.06.20
    /// - Add this class and singleton implementations. Considering the thread safety.
    /// </summary>
    public class TiAppService : IiEventListener
    {
        private string _appdatadir;
        private IiEventListener _notifylistener;

        // For threadsafe considerations
        private static TiAppService _instance = null;
        private static readonly object _padlock = new object();

        private TiAppScheService _sche;
        private TiAppIoService _xio;
        private TiAppCacheService _cache;
        private TiAppDataService _data;
        //private IiMsgRouterService _msgservice;
        private TiAppSettingService _settings;

        private TiAppService()
        {
            _notifylistener = null;
            _sche = new TiAppScheService(this);
            _xio = new TiAppIoService();
            _cache = new TiAppCacheService();
            _data = new TiAppDataService();
            _settings = new TiAppSettingService("config.ini");
        }

        public static TiAppService Instance
        {
            get
            {
                lock (_padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new TiAppService();
                    }
                    return _instance;
                }
            }
        }

        public TiAppIoService Xio { get => _xio;  }
        public TiAppScheService Sche { get => _sche; }
        public TiAppCacheService Cache{ get => _cache; }
        public bool IsDataDirty { get => _cache.IsDataDirty; }

        public void Open(IiEventListener listener)
        {
            _notifylistener = listener;
            _sche.SetEventListener(this);

            // For test purpose only
            _cache.IsDataDirty = true;
        }

        public void Close()
        {
            StopMeasurement();
            return;
        }

        public void StartMeasurement()
        {
            _sche.Start(this);
        }

        public void StopMeasurement()
        {
            // TODO: currently, it will raise a exception
            _sche.Stop();
        }

        public TiThreadScheService GetAppScheduler()
        {
            return _sche;
        }

        public TiAppIoService GetAppIoService()
        {
            return _xio;
        }

        /// <summary>
        /// Load data to satisfy the displaying and visualization requirement of the UI layer.
        /// 
        /// This method is called by the UI layer. It will fetch the data from internal cache
        /// or data service. The results will be placed into the buffer. The UI layer can 
        /// make use of these data for visualization/dynamic display purpose directly.
        /// 
        /// </summary>
        /// <param name="buf"></param>
        /// <returns>How many elements in the buffer.</returns>
        public int Read(ref int[] buf, int capacity)
        {
            int value = 0;
            int retvalue = 0;

            if (_cache.TryPop(out value))
            {
                buf[0] = value;
                retvalue = 1;
            }


            if (_cache.IsEmpty())
            {
                _cache.Modified = false;
            }

            return retvalue;
        }

        public TiScheMessage ReadMessage()
        {
            return _sche.Read();
        }

        /// <summary>
        /// Save the data inside the memory to persistent storage.
        /// </summary>
        public void Save()
        {
            _cache.IsDataDirty = false;
        }

        /// <summary>
        /// Demos how to process the event and call read method.
        /// The value of the member variable "notifylistener" in the scheduler service is actually
        /// point to the Evolve() method here.
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void Evolve(TiEvent e)
        {
            // deal with specific type event. The other type events are ignored automatically. 
            if (e != null)
            {
                _notifylistener.Evolve(e);
            }

            // The following source code can be understood to run for any event input.
            //
            if (!_sche.MsgQueue.IsEmpty())
            {
                // Demo:
                // You can process the msg here.
                //
                TiScheMessage msg = _sche.Read();
                if (msg != null)
                {
                    switch (msg.MsgId)
                    {
                        case TiFoundation.MSG_NOTIFY:
                            e = new TiEvent(TiFoundation.EVENT_SCHE_REPORT);
                            e.Source = this;
                            e.Param = msg.IntValue;
                            _notifylistener.Evolve(e);
                            break;

                        case TiFoundation.MSG_COMPLETED:
                        case TiFoundation.MSG_ERROR:
                        case TiFoundation.MSG_SCHEDULER_CANCELED:
                            break;
                    }
                }
            }

            if (_cache.Modified)
            {
                e = new TiEvent(TiFoundation.EVENT_DATA_READY, "cache data is ready for processing");
                _notifylistener.Evolve(e);
            }
        }

        public void Test()
        {

        }
    }
}
