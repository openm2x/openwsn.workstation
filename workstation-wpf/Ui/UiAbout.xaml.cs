﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace App.WorkStation.Ui
{
    /// <summary>
    /// UiAbout.xaml 的交互逻辑
    /// 
    /// Reference
    /// How to: Handle a Loaded Event, 2017, 
    /// https://docs.microsoft.com/en-us/dotnet/framework/wpf/advanced/how-to-handle-a-loaded-event
    /// 
    /// author zhangwei@TongJi University, 2020
    /// 
    /// </summary>
    public partial class UiAbout : Window
    {
        public UiAbout()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Contributor List in the year of 2020:\n");
            sb.Append("张三，同济大学自动化专业\n");
            sb.Append("李四，同济大学自动化专业\n");
            InformationText.Text = sb.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
