﻿using App.WorkStation.Service;
using App.WorkStation.Ui;
using App.Xvx.Common.Sche;
using App.Xvx.Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace workstation_wpf
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// 
    /// Reference
    /// WPF Windows Overview, 2017,
    /// https://docs.microsoft.com/en-us/dotnet/framework/wpf/app-development/wpf-windows-overview
    /// Close an Reopen a WPF Window possible ?
    /// https://social.msdn.microsoft.com/Forums/en-US/780c9f72-68c7-42ad-b6f0-81ed918b3073/close-an-reopen-a-wpf-window-possible-
    /// 
    /// author zhangwei@TongJi University, 2020
    /// 
    /// </summary>
    public partial class MainWindow : Window, IiEventListener
    {
        private TiAppService _avc;
        private UiTest _testwindow = null;

        public MainWindow()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            // Initialize the application service layer.
            _avc = TiAppService.Instance;
        }

        #region Core logic including event processing and Ui update.
        /// <summary>
        /// Implement the IiEventListener interface.
        /// Attention the Evolve() method is often invoked inanother thread. So it cannot update
        /// the variable and UI components directly because they belong to the UI thread. That's 
        /// why we must use the dispatcher here, and let the dispatcher (belongs to the UI thread)
        /// to execute the tasks.
        /// 
        /// Reference
        /// Dispatcher.Invoke 方法, 
        /// https://docs.microsoft.com/zh-cn/dotnet/api/system.windows.threading.dispatcher.invoke?view=netcore-3.1
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void Evolve(TiEvent e)
        {
            // Action<TiEvent> action = new Action<TiEvent>(_OnAppServiceEventReceived);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate { 
                _OnAppServiceEventReceived(e); 
            });
            // this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, action);
        }

        private void _OnAppServiceEventReceived(TiEvent e)
        {
            switch (e.Id)
            {
                case TiFoundation.EVENT_SCHE_REPORT:
                    TiScheMessage msg = null;
                    while (true)
                    {
                        msg = _avc.ReadMessage();
                        if (msg == null)
                            break;

                        _AppendDisplay(DateTime.Now.ToString() + " received message: " + msg.ToString());
                    }
                    break;

                case TiFoundation.EVENT_DATA_READY:
                    // Demo: this event indicate there're data for processing in the cache
                    // you should add your own processing here.
                    // Now we simply pass this event to the upper UI layer.
                    int[] values = new int[100];
                    int len = _avc.Read(ref values, values.Length);
                    if (len > 0)
                    {
                        _AppendDisplay(DateTime.Now.ToString() + " received data: ", values, len);
                    }
                    break;

                default:
                    // simply ignore all other events received.
                    break;
            }

        }

        private void _AppendDisplay(string s)
        {
            // 大于100行清除记录，可选
            if (OutputTextBlock.LineCount > 500)
            {
                OutputTextBlock.Clear();
            }
            OutputTextBlock.AppendText(s + "\n");

            // 自动滚动到底部
            OutputTextBlock.ScrollToEnd();

            // OutputTextBlock.Inlines.Add(s);
            // OutputTextBlock.Inlines.Add(new LineBreak());
        }

        private void _AppendDisplay(string prefix, int[] values, int len)
        {
            StringBuilder sb = new StringBuilder(prefix);
            sb.Append(':').Append(' ');
            for (int i=0; i<len; i++)
            {
                sb.Append(values[i].ToString()).Append(' ');
            }
            sb.Append('\n');
            OutputTextBlock.AppendText(sb.ToString());
            OutputTextBlock.ScrollToEnd();
        }
        #endregion

        // From here, are window events

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            _avc.Open(this);
            String s = "Welcome to come to the simulated sensor and workstation application in the embedded course 2020 at TongJi University!";
            OutputTextBlock.Text = s;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // If data is dirty, notify user and ask for a response
            // if (this.isDataDirty)
            if (true)
            {
                string msg = "Data is dirty. Close without saving?";
                MessageBoxResult result = MessageBox.Show(msg, "Data App", MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    // If user doesn't want to close, cancel closure
                    e.Cancel = true;
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            _avc.Close();
            Application.Current.Shutdown();
        }

        private void ExecuteButton_Click(object sender, RoutedEventArgs e)
        {
            _avc.StartMeasurement();

            ProgressBar.Minimum = 0;
            ProgressBar.Maximum = 100;
            ProgressBar.Value = 50;
        }

        private void TestButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("内容", "标题", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                // Demonstrate how to call the DLL method.
                int result = _avc.GetAppIoService().Sum(3, 5);
                _AppendDisplay("yes" + result.ToString());
            }
            else
            {
                _AppendDisplay("no");
            }
        }

        /// <summary>
        /// modified by zhangwei on 2020.06.27
        /// For the first version, the test window can open successfully but failed to reopen
        /// because the variable "_testwindow" is not null. 
        /// 
        /// Furthermore, I don't want to use Show/Hide to do this. So I use an delegation
        /// which can be triggered when the test window is closed. The variable "_testwindow"
        /// is set to null in the delegation. By this way, the test window can be reopened
        /// multiple times.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TestWindowButton_Click(object sender, RoutedEventArgs e)
        {
            // var testwindow = new UiTest(_avc);
            // testwindow.ShowDialog();

            if (_testwindow == null)
            {
                System.EventHandler ontestwindowclosed = delegate (object sender2, EventArgs e2)
                {
                    _testwindow = null;
                };
                _testwindow = new UiTest(_avc, ontestwindowclosed); 
            }
            _testwindow.Show();
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            var about = new UiAbout();
            about.ShowDialog();
        }

    }
}
