#include "api_configall.h"
#include "api_foundation.h"
#include "../service/avc_udpclient.h"
#include "../service/avc_udpserver.h"

static UDPClient* _client = NULL;

int __stdcall io_client_start()
{
	if (_client == NULL)
	{
		_client = new UDPClient();
	}
	return 1;
}

int __stdcall io_client_stop()
{
	if (_client != NULL)
	{
		delete _client;
		_client = NULL;
	}
	return 1;
}

int __stdcall io_client_recv(char* buf, int capacity, int option)
{
	assert(false);
	return -1;
}

int __stdcall io_client_send(const char * buf, int len, int option)
{
	assert(_client != NULL);
	_client->sendData(buf, len);
	return 1;
}
