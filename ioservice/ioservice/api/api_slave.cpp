#include "api_configall.h"
#include "api_foundation.h"
#include "../service/avc_udpecho2.h"

// https ://blog.csdn.net/weixin_42678507/java/article/details/91546401

int _stdcall io_slave_open()
{
    return 0;
}

int _stdcall io_slave_close()
{
    return 0;
}

int _stdcall io_slave_send(char* buf, int len, int option)
{
    return 0;
}

int _stdcall io_slave_recv(char* buf, int capacity, int option)
{
    return 0;
}




// The following version is for avc_udpecho.h 
/*
int io_slave_test()
{
    UdpServer server;

    // server.start("0", 9090, [](const std::string& req, std::string& resp)
    server.start("192.168.0.1", 9090, [](const std::string& req, std::string& resp)
    {
        resp = req;
    });
    return 0;
}
*/
int io_slave_test()
{
    char* argv[] = { "udpserver", "9003" };
    echo_server_execute(2, argv);
    return 0;
}
