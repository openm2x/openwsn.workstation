#include "api_configall.h"
#include "api_foundation.h"
// #include "../service/avc_udpecho.h"
#include "../service/avc_udpecho2.h"

// https ://blog.csdn.net/weixin_42678507/java/article/details/91546401


int _stdcall io_master_open()
{
    return 0;
}

int _stdcall io_master_close()
{
    return 0;
}

int _stdcall io_master_send(char* buf, int len, int option)
{
    return 0;
}

int _stdcall io_master_recv(char* buf, int capacity, int option)
{
    return 0;
}

// The following version is for avc_udpecho.h 
/*
int io_master_test()
{
    // UdpClient client("47.101.192.120", 9090);
    UdpClient client("192.168.0.1", 9090);

    std::cout << "欢迎使用回显服务器！" << std::endl;
    while (true)
    {
        std::cout << "请输入一段内容：";
        fflush(stdout);

        std::string req;
        std::getline(std::cin, req);

        client.SendTo(req);

        std::string resp;
        client.RecvFrom(resp);

        std::cout << resp.c_str() << std::endl;
    }
    return 0;
}
*/

int io_master_test()
{
    char* argv[] = { "udpclient", "127.0.0.1", "9003" };
    echo_client_execute(3, argv);
    return 0;
}
