#pragma once


#include <stdint.h>
#include <tchar.h> 


#ifdef __cplusplus
extern "C" {
#endif

int _stdcall io_master_open();
int _stdcall io_master_close();
int _stdcall io_master_send(char* buf, int len, int option);
int _stdcall io_master_recv(char* buf, int capacity, int option);

int __stdcall io_master_test();


#ifdef __cplusplus
}
#endif