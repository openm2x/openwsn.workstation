#pragma once


#include <stdint.h>
#include <tchar.h> 


#ifdef __cplusplus
extern "C" {
#endif

int _stdcall io_slave_open();
int _stdcall io_slave_close();
int _stdcall io_slave_send(char * buf, int len, int option);
int _stdcall io_slave_recv(char* buf, int capacity, int option);

int __stdcall io_slave_test();


#ifdef __cplusplus
}
#endif