﻿#pragma once

#include "api_configall.h"
#include "api_foundation.h"
#include "../service/avc_udpclient.h"
#include "../service/avc_udpserver.h"

// Reference
// C++ UDP C/S 简单封装, 2017,
// https://blog.csdn.net/qwertyupoiuytr/article/details/54861318
// 
// 封装UDP Socket API并且构建一个简单的回显服务器, 2019,
// https://blog.csdn.net/weixin_42678507/article/details/91546401?utm_medium=distribute.pc_relevant.none-task-blog-baidujs-2

#define MODE_SLAVE 1
#define MODE_MASTER 2

static int _mode = MODE_SLAVE;
static UDPServer * _server = NULL;

static const char* _destAddr = "127.0.0.1";
static const int _destPort = 4000;
static const char _charbuf[1024] = "This is a udp test message";
static UDPClient* _client = NULL;


// mode 1: server mode
// mode 2: client mode
//
int io_service_open(int mode)
{
    _mode = mode;

    switch (mode) {
    case 1:
        _server = new UDPServer;
        break;
    case 2:
        _client = new UDPClient;
        break;
    }

    return 0;
}

int io_service_close()
{
    switch (_mode) {
    case 1:
        delete _server;
        break;
    case 2:
        delete _client;
        break;
    }
    return 0;
}

int io_service_start(int port)
{
    _server->listen(4000);
    return 0;
}

int io_service_stop()
{
    // TODO
    return 0;
}

int io_service_connect(char * destAddr, int port)
{
    _client->setSockAddr(destAddr, _destPort);
    return 0;
}

int io_service_disconnect()
{
    return 0;
}


int io_service_recv(unsigned char* buf, int capacity, int option)
{
    for (int i = 0; i < 20; i++)
    {
        _server->recv();
    }
    return 0;
}


int io_service_send(const char* buf, int capacity, int option)
{
    for (int i = 0; i < 10; i++)

    {

        _client->sendData(buf, sizeof(buf));

    }
    return 0;
}




/*
//为了保证数字滤波器起到50Hz谐波陷波器作用，采样频率应取50Hz的整数倍,目前设为500hz
int filter_50hz(int * input, int inputlen, int * output, int outputlen, int samplerate)
{
    //todo
    assert(samplerate == 500);
    int currentPtr;
    for (currentPtr = 0; currentPtr < inputlen; currentPtr++)
    {
        if (currentPtr < 15)
            * (output + currentPtr) = *(input + currentPtr);
        else
        {
            *(output + currentPtr) = *(input + currentPtr - 1) + *(input + currentPtr - 5) * 5 + *(input + currentPtr - 10) * 5 - *(input + currentPtr - 15);
            *(output + currentPtr) >>= 3;
        }

    }
    return 0;
}
*/
