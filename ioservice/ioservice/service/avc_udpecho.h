#pragma once

#include "avc_configall.h"
#ifdef CONFIG_LINUX
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#else
#include <winsock2.h>
#include <sys/types.h>
#endif
#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include <cassert>
#include <functional>
#include "avc_foundation.h"

// Reference
// 封装UDP Socket API并且构建一个简单的回显服务器, 2019,
// https ://blog.csdn.net/weixin_42678507/java/article/details/91546401


class UdpSocket
{
public:
    UdpSocket()
        :fd_(-1)
    {}

    bool Socket()
    {
        fd_ = socket(AF_INET, SOCK_DGRAM, 0);

        if (fd_ < 0)
        {
            perror("socket");
            return false;
        }

        return true;
    }

    bool Close()
    {
        if (fd_ != -1)
        {
#ifdef CONFIG_LINUX
            ::close(fd_);
#else
            ::closesocket(fd_);
#endif
        }
        return true;
    }


    // 将fd_ 与 指定端口号绑定
    bool Bind(const std::string& ip, uint16_t port)
    {
        sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = inet_addr(ip.c_str()); // inet_addr 是将点分式 ip 转换成结构体需要的格式
        addr.sin_port = htons(port); // htons 是将主机字节序转换成网络字节序

        int ret = ::bind(fd_, (sockaddr*)&addr, sizeof(addr));
        if (ret < 0)
        {
            perror("bind");
            return false;
        }

        return true;
    }

    // 三个输出型参数
    // 1. 接收到了什么请求
    // 2. 对方的 ip 地址
    // 3. 对方的端口号
    bool RecvFrom(std::string& msg, std::string* ip = nullptr, uint16_t* port = nullptr)
    {
        sockaddr_in peer;
#ifdef CONFIG_LINUX
        socklen_t len = sizeof(peer);
#else
        int len = sizeof(peer);
#endif

        char buf[10 * 1024] = { 0 };

#ifdef CONFIG_LINUX
        ssize_t num = recvfrom(fd_, buf, sizeof(buf) - 1, 0,
#else
        int num = recvfrom(fd_, buf, sizeof(buf) - 1, 0,
#endif
            (sockaddr*)&peer, &len);
        if (num < 0)
        {
            perror("recvform");
            return false;
        }

        msg = buf;

        if (ip != nullptr)
        {
            *ip = inet_ntoa(peer.sin_addr);
        }
        if (port != nullptr)
        {
            *port = ntohs(peer.sin_port);
        }

        return true;
    }


    // 参数要求
    // 1. 发送的响应
    // 2. 对方的ip
    // 3. 对方的端口号
    bool SendTo(std::string& msg, std::string& ip, uint16_t port)
    {
        sockaddr_in peer;
        peer.sin_family = AF_INET;
        peer.sin_addr.s_addr = inet_addr(ip.c_str());
        peer.sin_port = htons(port);

#ifdef CONFIG_LINUX
        ssize_t num = sendto(fd_, msg.c_str(), msg.size(), 0,
            (sockaddr*)&peer, sizeof(peer));
#else
        int num = sendto(fd_, msg.c_str(), msg.size(), 0,
            (sockaddr*)&peer, sizeof(peer));
#endif
        if (num < 0)
        {
            perror("sendto");
            return false;
        }

        return true;
    }
private:
    int fd_;
};


// 一个可以接收函数指针和仿函数的通用的东西
typedef std::function<void(const std::string&, std::string&)> Handler;

class UdpServer
{
public:
    UdpServer()
    {
        sock_.Socket();
    }
    ~UdpServer()
    {
        sock_.Close();
    }

    bool start(const std::string& ip, uint16_t port, Handler handler)
    {
        // 先绑定
        int ret = sock_.Bind(ip, port);
        if (!ret)
        {
            perror("Bind");
            return false;
        }

        while (true)
        {
            std::string req;
            std::string peer_ip;
            uint16_t peer_port;
            // 1. 接收请求
            ret = sock_.RecvFrom(req, &peer_ip, &peer_port);
            if (!ret)
            {
                perror("RecvFrom");
                continue;
            }
            // 打印接收到的信息
            printf("[ip: %s --- port: %d]  %s\n", peer_ip.c_str(), peer_port, req.c_str());

            // 2. 处理请求
            std::string resp;
            handler(req, resp);

            // 3. 响应请求
            sock_.SendTo(resp, peer_ip, peer_port);
        }
    }

private:
    UdpSocket sock_;
};

class UdpClient
{
public:
    UdpClient(const std::string& ip, const uint16_t port)
        :server_ip_(ip),
        server_port_(port)
    {
        sock_.Socket();
    }

    ~UdpClient()
    {
        sock_.Close();
    }

    bool RecvFrom(std::string& msg)
    {
        return sock_.RecvFrom(msg);
    }

    bool SendTo(std::string& msg)
    {
        return sock_.SendTo(msg, server_ip_, server_port_);
    }
private:
    UdpSocket sock_;
    std::string server_ip_;
    uint16_t server_port_;
};

