/**
 * @author zhangwei in 2011.08
 * - Compiled successully.
 * @modified by Ning Huaqiang in 2011.08
 * - Revision.
 * @modified by huangmin in 2016.0503
 * - Revision.
 */

// Reference
// C++ UDP C/S �򵥷�װ, 2017,
// https://blog.csdn.net/qwertyupoiuytr/article/details/54861318

// for MSVC: disable CRT function warnings. It's only effective for the current file 
// and recognized by MSVC compiler.

//#define _CRT_SECURE_NO_WARNINGS

#include "avc_configall.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h> 
#include <time.h>  
#include <assert.h>
#include <iostream>
#include <Winsock2.h>
#include "avc_foundation.h"
#include "avc_udpserver.h"

// Reference
// https ://blog.csdn.net/qwertyupoiuytr/java/article/details/54861318
//
// multithreaded - UDP - client - server
// https://github.com/pranavgupta21/multithreaded-UDP-client-server/blob/master/server/thread.c

using namespace std;

#pragma comment(lib, "ws2_32.lib")


UDPServer::UDPServer()
{
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	serverSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	cout << "Server Socketinitialized" << endl;
}

UDPServer::~UDPServer()
{
	closesocket(serverSocket);
	WSACleanup();
	cout << "Server Socketreleased" << endl;
}

void UDPServer::listen(const int port)
{
	serverSockAddr.sin_family = AF_INET;
	serverSockAddr.sin_port = htons(port);
	serverSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(serverSocket, (SOCKADDR*)&serverSockAddr, sizeof(serverSockAddr));
}

void UDPServer::recv() const
{
	char buf[1024];
	int serverSockAddrSize = sizeof(serverSockAddr);
	recvfrom(serverSocket, buf, sizeof(buf), 0, (SOCKADDR*)&serverSockAddr, &serverSockAddrSize);
	cout << buf << endl;
}

int UDPServer::recv(char * buf, int capacity, int option) 
{
	// char buf[1024];
	int serverSockAddrSize = sizeof(serverSockAddr);
	return recvfrom(serverSocket, buf, capacity, 0, (SOCKADDR*)&serverSockAddr, &serverSockAddrSize);
}



#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif
