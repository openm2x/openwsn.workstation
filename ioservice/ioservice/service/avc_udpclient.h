#ifndef _AVC_UDPCLIENT_H_2143_
#define _AVC_UDPCLIENT_H_2143_


#include "avc_configall.h"
#include<Winsock2.h>
#include "avc_foundation.h"

// Reference
// https ://blog.csdn.net/qwertyupoiuytr/java/article/details/54861318

class UDPClient {
private:
	WSADATA wsaData;
	SOCKET clientSocket;
	sockaddr_in clientSockAddr;
public:
	UDPClient();
	void setSockAddr(const char* destAddr, const int destPort);
	void sendData(const char* buf, const int len) const;
	~UDPClient();
};

#endif