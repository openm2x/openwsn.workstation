
#include <windows.h>
#include <stdio.h>

void test02master()
{
	// typedef int (__stdcall *TiFunSumPtr)(int a, int b);
	typedef int(__stdcall* TiFunIoMasterTest)();

	HINSTANCE instance;
	// Load dLL dynamically
	instance = LoadLibrary(L"ioservice.dll");

	// GetProcAddress 获取该函数的地址
	TiFunIoMasterTest io_master_test = (TiFunIoMasterTest)GetProcAddress(instance, "io_master_test");
	int result = io_master_test();
	printf("The result from dll is: %d", result);
	getchar();
}