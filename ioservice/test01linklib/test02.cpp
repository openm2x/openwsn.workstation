
#include <windows.h>
#include <stdio.h>

void test02()
{
	typedef int(__stdcall *TiFunSumPtr)(int a, int b);

	HINSTANCE instance;
	// Load dLL dynamically
	instance = LoadLibrary(L"ioservice.dll"); 

	// GetProcAddress 获取该函数的地址
	TiFunSumPtr sum = (TiFunSumPtr)GetProcAddress(instance, "dll_api_test01"); 
	int result = sum(3, 5);
	printf("The result from dll is: %d", result);
	getchar();
}
